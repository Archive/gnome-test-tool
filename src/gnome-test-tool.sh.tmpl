#!/bin/sh

NAME=`basename $0`
DATE=`date +"%m.%d.%y - %H:%M:%S"`
HOSTNAME=`uname -n`
OSNAME=`uname `
OSRELEASE=`uname -r`
#OSREL=`cat /etc/release | grep Solaris | sed 's/^ *//'`

GERD_WORKING_DIR=`pwd`
GERD_EXTRA_ARGS=
GERD_SNAPSHOT_DIR=${GERD_SNAPSHOT_DIR:-`pwd`}
GERD_BASELINE_DIR=${GERD_BASELINE_DIR:-`pwd`}
GERD_RESULTS_FILE=${GERD_RESULTS_FILE:-"gnome-test-tool-play.res"}
GERD_RESULTS="$GERD_WORKING_DIR/$GERD_RESULTS_FILE"
GERD_APP_DIR=${GERD_RESULTS_FILE:-"/opt/gnome-2.0/bin/"}
GERD_SIGNALS=${GERD_SIGNALS:-"SIGSEGV SIGBUS SIGQUIT SIGABRT SIGILL"}

usage_message()
{
  	case "$NAME" in

    *-play)
        echo "usage: gnome-test-tool-play --event-file/-e <event-file> <application>"
        echo "                 [--snapshot-dir/-s <snapshot_dir>]"
#       echo "                 [--baseline-dir/-b <baseline-dir>]"
        echo "                 [--results-file/-r <results-file>]"
        echo "                 [--playback-delay|-p <playback_delay>]"
        echo "                 [--help/-h] [--version/-v]"
	;;

    *-record)
        echo "usage: gnome-test-tool-record --event-file/-e <event-file> <application>"
        echo "                 [--snapshot-dir/-s <snapshot_dir>]"
        echo "                 [--nowait/-n]"
        echo "                 [--nowarp/-o]"
        echo "                 [--playback-delay|-p <playback_delay>]"
        echo "                 [--help/-h] [--version/-v]"
	;;

	esac
}

convert_full_path()
{
	res=`echo "$1" | cut -c1` 
	if [ "$res" = "/" ] ; then
		res="$1"
    else
		res="$GERD_WORKING_DIR/$1"
	fi  
}

print_help_version()
{
 FILE=temp
 gtt-launch $GERD_RESULTS $FILE $APP $GERD_EXTRA_ARGS
 exit 0 
}

while [ -n "$1" ] ; do

  case "$1" in

    --help|-h)
	if [ -z "$APP" ] ; then
	 	FILE=
		APP=gtt-gtkdummy
		GERD_EXTRA_ARGS=--gnome-test-tool-help
		print_help_version
	else
	  GERD_EXTRA_ARGS=`echo "$GERD_EXTRA_ARGS $1"`
	fi
 	
	shift;;

    --version|-v)
	if [ -z "$APP" ] ; then
		FILE=
		APP=gtt-gtkdummy
		GERD_EXTRA_ARGS=--gnome-test-tool-version
		print_help_version
	else
	  GERD_EXTRA_ARGS=`echo "$GERD_EXTRA_ARGS $1"`
	fi
	shift;;

    --event-file|-e)
	FILE="$2"
	convert_full_path $2
	FILE=$res	
	shift 2;;

    --snapshot-dir|-s)
	GERD_SNAPSHOT_DIR="$2"
	if [ ! -d "$GERD_SNAPSHOT_DIR" ] ; then
	  mkdir -p "$GERD_SNAPSHOT_DIR"
	  if [ $? -ne 0 ] ; then
	    echo "${NAME}: Failed to create snapshot dir $GERD_SNAPSHOT_DIR"
	    exit 1
	  fi
	fi
	convert_full_path $2
	GERD_SNAPSHOT_DIR="$res"
	shift 2;;

    --baseline-dir|-b)
	GERD_BASELINE_DIR="$2"
	if [ ! -d "$GERD_BASELINE_DIR" ] ; then
	  echo "${NAME}: Baseline dir $GERD_BASELINE_DIR does not exist"
	  exit 1
	fi
	convert_full_path $2
	GERD_BASELINE_DIR="$res"
	shift 2;;

    --results-file|-r)
	GERD_RESULTS="$2"
	convert_full_path $2
	GERD_RESULTS="$res"
	shift 2;;

    --nowait|-n)
	GERD_NOWAIT=1
	export GERD_NOWAIT
	shift;;

    --nowarp|-o)
	GERD_NOWARP=1
	export GERD_NOWARP
	shift;;

    --playback-delay|-p)
	GERD_PLAYBACK_DELAY="$2"
	export GERD_PLAYBACK_DELAY
	shift 2;;

    *)
	if [ -n "$APP" ] ; then
	  GERD_EXTRA_ARGS=`echo "$GERD_EXTRA_ARGS $1"`
	else
	  APP="$1"
	fi
	shift;;

  esac

done


if [ -z "$FILE" ] ; then
	usage_message
	exit 1
fi

if [ -z "$APP" ] ; then
	APP="gnome-terminal"
fi

# Check whether the specified application is present or not
if [ ! `which $APP` ] ; then
  echo "Specified application is not available"
  exit 1
elif [ ! -x `which $APP` ] ; then
  echo "${NAME}: $APP is not executable"
  exit 1
fi

APP=`which $APP`

case "$NAME" in

  *-play)
# Check whether the specified event file is present or not
	if [ ! -f "$FILE" ] ; then
	  echo "Specify a correct file for reading"
	  exit 1
	fi

	if [ ! -r "$FILE" ] ; then
	  echo "Unable to read the event file for playing"
	  exit 1
	fi

	touch $GERD_RESULTS

# Check whether the user has permissions to access the specified result file.
	if [ $? -ne 0 ] ; then
		echo "Unable to open the results file"
	    exit 1
	fi

# Check whether the specified Gerd results file is a "file".
	if [ ! -f "$GERD_RESULTS" ] ; then
	  echo "Specfied only a directory, specify a file for writing the results of the playback"
	  exit 1
	fi

	if [ ! -w "$GERD_RESULTS" ] ; then
	  echo "Unable to open the results file"
	  exit 1
	fi

	echo "GTT Test Results" >> $GERD_RESULTS
	echo >> $GERD_RESULTS
	echo "Application Under Test	: $APP" >> $GERD_RESULTS
	echo >> $GERD_RESULTS
	echo "Event File				: $FILE" >> $GERD_RESULTS
	echo "Baseline directory		: $GERD_BASELINE_DIR" >> $GERD_RESULTS
	echo "Snapshot directory		: $GERD_SNAPSHOT_DIR" >> $GERD_RESULTS
	echo >> $GERD_RESULTS
	echo "Machine					: $HOSTNAME" >> $GERD_RESULTS
    echo "OS/Release				: $OSNAME/$OSRELEASE" >> $GERD_RESULTS
	echo "Test Date				: $DATE" >> $GERD_RESULTS
	echo >> $GERD_RESULTS

	GERD_PLAY="$FILE"

	;;

  *-record)
	echo > $FILE

	if [ $? -ne 0 ] ; then
		echo "Unable to open the event file for recording"
	    exit 1
	fi

	GERD_RECORD="$FILE"
	;;

  *)
	echo "$NAME: call gnome-test-tool-play or gnome-test-tool-record"
	exit 1
	;;
esac


GTK_MODULES=$GTK_MODULES:gtt

GERD_PROG="$APP"

if [ -f ".GERD_CORE" ] ; then
  rm -f .GERD_CORE
fi

export GERD_PLAY GERD_RECORD GERD_RESULTS GTK_MODULES GERD_EXTRA_ARGS
export GERD_PROG GERD_BASELINE_DIR GERD_SNAPSHOT_DIR GERD_SIGNALS GERD_WORKING_DIR

LD_PRELOAD=@libdir@/libgtt-xwrapper.so $@

gtt-launch $GERD_RESULTS $FILE $APP $GERD_EXTRA_ARGS

if [ -f ".GERD_CORE" ] ; then
  SUBTEST_NAME=`head -1 .GERD_CORE`
  SUBTEST_NAME=`echo $SUBTEST_NAME | sed 's/ /_/g'`
  if [ -f core ] ; then
    mv core ${SUBTEST_NAME}.core
  else
    echo "${NAME}: core file not found"
  fi
  rm -f .GERD_CORE
fi

