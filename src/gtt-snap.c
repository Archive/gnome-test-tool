/* Gnome_test_tool
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Intrinsic.h>
#include <X11/XWDFile.h>

#include <gdk/gdk.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gdk-pixbuf/gdk-pixdata.h>
#include <png.h>

#include "gtt-common.h"
#include "gtt-events.h"
#include "gtt-io.h"
#include "gtt-xwrapper.h"
#include "gtt-snap.h"
#include "gtt-client.h"

#define lowbit(x) ((x) & (~(x) + 1))

/* --- prototypes --- */
static void widget_dump (GtkWidget *widget, FILE *handle);
static void print_arguments (GtkObject *object, FILE *handle);
static void write_arguments	(GtkObject *object, GParamSpec *param, guint i, FILE *handle);

/* --- variables --- */
static gchar *snap_exts[3] = { ".gtt", ".png", ".snp" };
static gchar *baseline_dir = ".";
static gchar *snapshot_dir = ".";

/* --- functions --- */
#include <gdk/gdkx.h>

void
gerd_snapshot_snp_get (GdkWindow *window, gchar *filename)
{
    FILE *handle;
    GtkWidget *widget;
    gchar *filepath;

    g_return_if_fail (window != NULL);

    filepath = g_strdup_printf ("%s/%s", snapshot_dir, filename);

    handle = fopen (filepath, "wb");
    if (handle == NULL) {
        printf ("ERROR - unable to open [%s]\n", filepath);
        g_free (filepath);
        return;
    }

    gdk_window_get_user_data (window, (gpointer) &widget);
    widget_dump (widget, handle);

    fclose (handle);
    g_free (filepath);
}

gint
gerd_snapshot_snp_compare (gchar *basefile, gchar *newfile, GString **err_str)
{
    gchar *outfile;
    gchar *syscmd;
    gint result;

    outfile = tempnam (g_get_tmp_dir (), "gerddiff");

    syscmd = g_strdup_printf ("diff %s/%s %s/%s > %s 2>&1",
			baseline_dir, basefile, snapshot_dir, newfile, outfile);

    result = system (syscmd);
    g_free (syscmd);

    if (result != 0)
    {
      FILE *fp;
      gchar line[1024];

      *err_str = g_string_new ("");

      if ((fp = fopen (outfile, "r")) == NULL)
        g_string_printf (*err_str, "- can't open diff file %s", outfile);
      else
      {
        while (fgets (line, 1024, fp))
          g_string_append_printf (*err_str, "%s", line);
        unlink (outfile); 
      }
    }
    return (result);
}

static void
widget_dump (GtkWidget *widget, FILE *handle)
{
    gchar *win_path;

    g_return_if_fail (widget != NULL);

    win_path = gerd_window_create_path (widget->window);
    fwrite (win_path, strlen (win_path), 1, handle);
    fwrite ("\n\n", strlen ("\n\n"), 1, handle);
    g_free (win_path);

    print_arguments (GTK_OBJECT (widget), handle);

    if (GTK_IS_CONTAINER (widget)) {
        GList *widget_list, *elem;

        widget_list = gtk_container_get_children (GTK_CONTAINER(widget));
        elem = g_list_first (widget_list);

        while (elem != NULL)
        {
            widget_dump (GTK_WIDGET (elem->data), handle);

            elem = elem->next;
        }

        g_list_free (widget_list);
    }
}


static void
print_arguments(GtkObject* object, FILE *handle)
{
  GType type;
  gboolean has_focus_flag = FALSE;

  type = GTK_OBJECT_TYPE(object);

  do 
  {
    GParamSpec **param;
    guint n_args;
    guint i;
    gint is_focus_place = -1;
	guint prop_count;  
    gchar *type_str;

    param = g_object_class_list_properties (G_OBJECT_GET_CLASS (object),
											&n_args);
    
    if (n_args)
    {
      type_str = g_strdup_printf ("Object Type:\t\t`%s'\n",
      g_type_name (type) ? g_type_name(type) : "NULL");
      fwrite (type_str, strlen (type_str), 1, handle);
      g_free (type_str);
    }
   
    i = 0;
	prop_count = 0;
	is_focus_place = -1;
    while (i < n_args)
    {
	  if (!strcmp (param[i]->name, "is-focus") && !has_focus_flag)
	  {
		is_focus_place = i++;
		continue;
	  }
	  
	  write_arguments (object, param[i], prop_count++, handle);

	  if (!strcmp (param[i]->name, "has-focus"))
	  {
	    has_focus_flag = TRUE;

		if (is_focus_place >= 0)
		{
		write_arguments (object, param[is_focus_place], prop_count++, handle);
	    has_focus_flag = FALSE;
		is_focus_place = -1;
		}
	  }
    
	  ++i;
	} 

    type = g_type_parent(type);
  }
  while (type != G_TYPE_INVALID);

}

static void write_arguments (GtkObject *object, GParamSpec *param, guint i, FILE *handle)
{

    GValue *value = NULL;
    GString *gstring;
    const gchar *prop_name;

    value = g_new0 (GValue, 1);
    g_value_init (value, param->value_type);
    prop_name = param->name;

	
    if (param->flags != G_PARAM_WRITABLE && 
		(param->value_type == G_TYPE_ULONG ||
	     param->value_type == G_TYPE_LONG ||
	     param->value_type == G_TYPE_ENUM ||
	     param->value_type == G_TYPE_UINT ||
	     param->value_type == G_TYPE_INT ||
	     param->value_type == G_TYPE_STRING ||
	     param->value_type == G_TYPE_BOOLEAN  ) )
	  {

	    gstring = g_string_new("");

	    g_object_get_property( G_OBJECT(object),prop_name, value );
	 
	    g_string_append_printf (gstring, "\t%u\t%s\t%s", i,
                              prop_name ? prop_name : "NULL",
                              g_type_name(param->value_type) ? 
                              g_type_name(param->value_type) : "NULL");

	    switch( G_VALUE_TYPE(value) ) {
		  case G_TYPE_ULONG:
			g_string_append_printf (gstring, "\t\t[%lu]\n", g_value_get_ulong(value));
				break;	
		  case G_TYPE_LONG:
				g_string_append_printf (gstring, "\t\t[%ld]\n", g_value_get_long(value));
				break;	
		  case G_TYPE_ENUM:
				g_string_append_printf (gstring, "\t\t[%d]\n", g_value_get_enum(value));
				break;	
		  case G_TYPE_UINT:
				g_string_append_printf (gstring, "\t\t[%u]\n", g_value_get_uint(value));
				break;	
		  case G_TYPE_INT:
				g_string_append_printf (gstring, "\t\t[%d]\n", g_value_get_int(value));
				break;	
		  case G_TYPE_STRING:
				g_string_append_printf (gstring, "\t\t[%s]\n", g_value_get_string(value) ? g_value_get_string(value) : "NULL");
				break;	
		  case G_TYPE_BOOLEAN:
				g_string_append_printf (gstring, "\t\t[%s]\n", g_value_get_boolean(value) ? "TRUE" : "FALSE");
				break;	
		  default:
				g_string_append_printf (gstring, "\t\t[NULL]\n");

		}
		

		fwrite (gstring->str, strlen (gstring->str), 1, handle);
		g_string_free (gstring, TRUE);

	}
    
	g_free(value);
}

void 
gerd_snapshot_png_get (GdkWindow *window, gchar *filename)
{
    gint width, height, depth, rowstride;
    gint i;
    guchar *pixels;
    guchar *buffer;
    gboolean has_alpha;
    FILE *handle;
    png_structp png_ptr;
    png_infop info_ptr;
    png_text text[2];
    GdkPixbuf *pixbuf;
    gchar *filepath;

    XWindowAttributes attr;
    GtkWidget *widget;
    gint x = 0 , y = 0 ;

   gdk_drawable_get_size (window, &width, &height);

   XGetWindowAttributes (GDK_DISPLAY (), GDK_WINDOW_XWINDOW (window), &attr);

   /* Input only window */
   if (attr.depth == 0)
   {
        gdk_window_get_user_data (window, (void **) &widget);

        if (!widget)
            return;

        window = widget->window;
        x = attr.x;
        y = attr.y;
    }

    pixbuf = gdk_pixbuf_new (GDK_COLORSPACE_RGB, FALSE, 8, width, height);
    pixbuf = gdk_pixbuf_get_from_drawable (pixbuf,
                                           (GdkDrawable *)window,
                                           (GdkColormap *)NULL,
                                           x,
                                           y,
                                           0,
                                           0,
                                           width,
                                           height);

    if (pixbuf == NULL) {
        printf ("ERROR - failed to get pixbuf\n");
        return;
    }

    filepath = g_strdup_printf ("%s/%s", snapshot_dir, filename);

    handle = fopen (filepath, "wb");
    if (handle == NULL) {
        printf ("ERROR - unable to open [%s]\n", filename);
        g_free (filepath);
        return;
    }

    png_ptr = png_create_write_struct (PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (png_ptr == NULL) {
        printf ("ERROR - failed to cread png write struct\n");
        fclose (handle);
        g_free (filepath);
        return;
    }

    info_ptr = png_create_info_struct (png_ptr);
    if (info_ptr == NULL) {
        printf ("ERROR - failed to cread png info struct\n");
        png_destroy_write_struct (&png_ptr, (png_infopp)NULL);
        fclose (handle);
        g_free (filepath);
        return;
    }

/*
    if (setjmp (png_ptr->jmpbuf)) {
        printf ("ERROR - failed to setjmp\n");
        png_destroy_write_struct (&png_ptr, (png_infopp)NULL);
       fclose (handle);
        g_free (filepath);
        return;
    }
*/

    png_init_io (png_ptr, handle);

    has_alpha = gdk_pixbuf_get_has_alpha (pixbuf);
    width = gdk_pixbuf_get_width (pixbuf);
    height = gdk_pixbuf_get_height (pixbuf);
    depth = gdk_pixbuf_get_bits_per_sample (pixbuf);
    pixels = gdk_pixbuf_get_pixels (pixbuf);
    rowstride = gdk_pixbuf_get_rowstride (pixbuf);


    /*g_message ("***** => pixmap:  %d x %d x %d, rowstride = %d\n", width, height, depth, rowstride);
*/
   	png_set_IHDR (png_ptr, info_ptr, width, height,
					depth, PNG_COLOR_TYPE_RGB_ALPHA,
					PNG_INTERLACE_NONE,
					PNG_COMPRESSION_TYPE_DEFAULT,
					PNG_FILTER_TYPE_DEFAULT);
    
	/* Some text to go with the png image */
    text[0].key = "Title";
    text[0].text = filename;
    text[0].compression = PNG_TEXT_COMPRESSION_NONE;
    text[1].key = "Software";
    text[1].text = "GERD";
    text[1].compression = PNG_TEXT_COMPRESSION_NONE;
    png_set_text (png_ptr, info_ptr, text, 2);

    /* Write header data */
    png_write_info (png_ptr, info_ptr);

    if (has_alpha)
      buffer = NULL;
    else
      buffer = g_malloc (4 *width);

    for (i = 0; i < height; i++) {

        if (has_alpha)
        {
           png_bytep row_pointer = pixels;
           png_write_row (png_ptr, row_pointer);
        }
        else
        {
	       int x;
	       guchar *buffer_ptr = buffer;
           guchar *source_ptr = pixels;
     	   for (x = 0; x < width; x++)
	   		{
				*buffer_ptr++ = *source_ptr++;	
				*buffer_ptr++ = *source_ptr++;	
				*buffer_ptr++ = *source_ptr++;	
				*buffer_ptr++ = 255;	
	    	}
           png_write_row (png_ptr, (png_bytep) buffer);

         }
         pixels += rowstride;
    }
    png_write_end (png_ptr, info_ptr);
    png_destroy_write_struct (&png_ptr, &info_ptr);
    g_free (buffer);
    fclose (handle);
    g_free (filepath);
}

gboolean
gerd_snapshot_png_compare (gchar *basefile, gchar *newfile, GString **err_str)
{
    GdkPixbuf *basepb, *newpb;
    guchar *basepixels, *newpixels;
    gint width, height, rowstride, i, j, badpixels = 0;
    gchar *basefilepath, *newfilepath;
    GError *error = NULL;

    basefilepath = g_strdup_printf ("%s/%s", baseline_dir, basefile);
    newfilepath = g_strdup_printf ("%s/%s", snapshot_dir, newfile);

    *err_str = g_string_new ("");

    basepb = gdk_pixbuf_new_from_file (basefilepath, &error);

    if (basepb == NULL) {
        g_string_printf (*err_str, "- can't read pixbuf from %s", basefilepath);
        g_free (basefilepath);
        return FALSE;
    }

    newpb = gdk_pixbuf_new_from_file (newfilepath, &error);

    if (newpb == NULL) {
        g_string_printf (*err_str, "- can't read pixbuf from %s", newfilepath);
        g_free (newfilepath);
        return FALSE;
    }

    width = gdk_pixbuf_get_width (basepb);
    if (width != gdk_pixbuf_get_width (newpb)) {
        g_string_printf (*err_str, "%s and %s - not the same width", basefile, newfile);
        return FALSE;
    }

    height = gdk_pixbuf_get_height (basepb);
    if (height != gdk_pixbuf_get_height (newpb)) {
        g_string_printf (*err_str, "%s and %s - not the same height", basefile, newfile);
        return FALSE;
    }

    rowstride = gdk_pixbuf_get_rowstride (basepb);
    if (rowstride != gdk_pixbuf_get_rowstride (newpb)) {
        g_string_printf (*err_str, "%s and %s - not the same rowstride", basefile, newfile);
        return FALSE;
    }

    basepixels = gdk_pixbuf_get_pixels (basepb);
    newpixels = gdk_pixbuf_get_pixels (newpb);

    for (i = 0; i < height; i++) {
        for (j = 0; j < rowstride; j++) {
            if (basepixels[j] != newpixels[j]) {
                 /* g_message ("[%s] and [%s] - differ at row %d, pixel %d: %x vrs %x\n", basefile, newfile, i, j, (gint) basepixels[j], (gint) newpixels[j]);  */

/*
                return FALSE; */
                badpixels++;
            }

        }

        basepixels += rowstride;
        newpixels += rowstride;
    }

    if (badpixels != 0)
    {
      g_string_printf (*err_str, "- %s and %s - differ by %d pixels",
                       basefile, newfile, badpixels);
      return FALSE;
    }

    /* if we got this far there were no errors so free err_str */
    g_string_free (*err_str, TRUE);
    *err_str = NULL;

    return TRUE;
}

void
gerd_snapshot_gtt_get (GdkWindow *window, gchar *filename)
{
    int width, height;
    FILE *handle;
    GdkPixbuf *pixbuf;
    gchar *filepath;
    GdkPixdata pixdata;
    guint8 *pixdata_serialized_data;
    guint pixdata_serialized_stream_length;
    GdkImage *image;

    XWindowAttributes attr;
    GtkWidget *widget;
    gint x = 0 , y = 0 ;

   gdk_drawable_get_size (window, &width, &height);

   XGetWindowAttributes (GDK_DISPLAY (), GDK_WINDOW_XWINDOW (window), &attr);

   /* Input only window */
   if (attr.depth == 0)
   {
        gdk_window_get_user_data (window, (void **) &widget);

        if (!widget)
            return;

        window = widget->window;
        x = attr.x;
        y = attr.y;
    }

    image = gdk_drawable_get_image	((GdkDrawable *)window, 
					x, 
					y, 
					width, 
					height);
    pixbuf = gdk_pixbuf_get_from_image	(NULL, 
					image, 
					(GdkColormap *)NULL, 
					0, 
					0, 
					0,
					0,
					width,
					height);

    if (pixbuf == NULL) {
        printf ("ERROR - failed to get pixbuf\n");
        return;
    }

    filepath = g_strdup_printf ("%s/%s", snapshot_dir, filename);

    handle = fopen (filepath, "wb");
    if (handle == NULL) {
        printf ("ERROR - unable to open [%s]\n", filename);
        g_free (filepath);
        return;
    }

    gdk_pixdata_from_pixbuf(&pixdata, pixbuf,FALSE);
    pixdata_serialized_data = gdk_pixdata_serialize(&pixdata, 
					&pixdata_serialized_stream_length);
    if (pixdata_serialized_data == NULL) {
        printf ("ERROR - failed to create serialized data from pixdata\n");
        fclose (handle);
        g_free (filepath);
        return;
    }

    if (fwrite(pixdata_serialized_data,
				sizeof(guint8), 
				pixdata_serialized_stream_length,
				handle)  
				!= pixdata_serialized_stream_length) {
        printf ("ERROR - failed to write complete serialized data into file\n");
        fclose (handle);
        g_free (filepath);
        g_free(pixdata_serialized_data);
        return;
    }

    fclose (handle);
    g_free (filepath);
    g_free(pixdata_serialized_data);
    if (pixbuf)
        g_object_unref(G_OBJECT(pixbuf));
}

gboolean
gerd_snapshot_gtt_compare (gchar *basefile, gchar *newfile, GString **err_str)
{
    GdkPixbuf *basepb, *newpb;
    gint width, height, rowstride, i, j, badpixels = 0;
    gchar *basefilepath, *newfilepath;
    FILE *basefile_handle, *newfile_handle;
    GError *error = NULL;

    GdkPixdata basep_pixdata, newp_pixdata;
    guint8 *basep_serialized_data, *newp_serialized_data;
    struct stat basefile_buf, newfile_buf;
    guint8 *basepixels, *newpixels;

    basefilepath = g_strdup_printf ("%s/%s", baseline_dir, basefile);
    newfilepath = g_strdup_printf ("%s/%s", snapshot_dir, newfile);

    printf("basefile = %s newfile = %s\n", basefile, newfile);

    basefile_handle = fopen (basefilepath, "rb");
    if (basefile_handle == NULL) {
    g_string_printf (NULL, "ERROR - unable to open [%s/%s]\n", baseline_dir, basefile);
    g_free (basefilepath);
    return FALSE;
    }

    newfile_handle = fopen (newfilepath, "rb");
    if (newfile_handle == NULL) {
        g_string_printf (NULL, "ERROR - unable to open [%s/%s]\n", snapshot_dir, newfile);
        g_free (newfilepath);
        return FALSE;
    }

    stat(basefilepath, &basefile_buf);
    stat(newfilepath, &newfile_buf);

    g_string_printf(NULL, "bs size %d, nfs size %d\n", 
			basefile_buf.st_size, newfile_buf.st_size);

    basep_serialized_data = (guint8 *)malloc(basefile_buf.st_size);
    if (fread(basep_serialized_data, 
			sizeof(guint8), 
			basefile_buf.st_size, 
			basefile_handle) != basefile_buf.st_size) {
        g_string_printf (NULL, "ERROR - unable to convert into serialized data from file [%s/%s]\n", baseline_dir, basefile);
        g_free (basep_serialized_data);
        g_free (basefilepath);
        g_free (newfilepath);
        return FALSE;
    }

    newp_serialized_data = (guint8 *)malloc(newfile_buf.st_size);
    if (fread(newp_serialized_data, 
			sizeof(guint8), 
			newfile_buf.st_size, 
			newfile_handle) != newfile_buf.st_size) {
        g_string_printf (NULL, "ERROR - unable to convert into serialized data from file [%s/%s]\n", snapshot_dir, newfile);
        g_free (newp_serialized_data);
        g_free(basep_serialized_data);
        g_free (basefilepath);
        g_free (newfilepath);
        return FALSE;
    }

    *err_str = g_string_new ("");

    if (!gdk_pixdata_deserialize(&basep_pixdata, 
				basefile_buf.st_size,
				basep_serialized_data,
				NULL)) {
        g_string_printf (NULL, "ERROR - unable to create pixdata from serialized data for file [%s/%s]\n", baseline_dir, basefile);
        g_free (newp_serialized_data);
        g_free(basep_serialized_data);
        g_free (basefilepath);
        g_free (newfilepath);
        return FALSE;
    }

    if (!gdk_pixdata_deserialize(&newp_pixdata, 
				newfile_buf.st_size,
				newp_serialized_data,
				NULL)) {
        g_string_printf (NULL, "ERROR - unable to create pixdata from serialized data for file [%s/%s]\n", snapshot_dir, newfile);
        g_free (newp_serialized_data);
        g_free(basep_serialized_data);
        g_free (basefilepath);
        g_free (newfilepath);
        return FALSE;
    }

    if(basep_pixdata.width != newp_pixdata.width) {
        g_string_printf (*err_str, "%s and %s - not the same width", 
				basefile, newfile);
        return FALSE;
    }

    if(basep_pixdata.height != newp_pixdata.height) {
        g_string_printf (*err_str, "%s and %s - not the same height", 
				basefile, newfile);
        return FALSE;
    }

    if(basep_pixdata.rowstride != newp_pixdata.rowstride) {
        g_string_printf (*err_str, "%s and %s - not the same rowstride", 
				basefile, newfile);
        return FALSE;
    }
    g_string_printf(*err_str, "height %d rowstride %d width %d\n", 
			newp_pixdata.height, 
			newp_pixdata.rowstride,
			newp_pixdata.width);

    basepixels = basep_pixdata.pixel_data;
    newpixels = newp_pixdata.pixel_data;

    for (i = 0; i < newp_pixdata.height; i++) {
        for (j = 0; j < newp_pixdata.width*3; j++) {
            if (basepixels[j] != newpixels[j]) {
        /*  g_message ("[%s] and [%s] - differ at pixel %d: %x vrs %x\n", basefi
le, newfile, i, (gint) basep_pixdata.pixel_data[i], (gint) newp_pixdata.pixel_da
ta[i]); */
                badpixels++;
            }
    }

     basepixels += newp_pixdata.rowstride;
     newpixels += newp_pixdata.rowstride;

    }

    g_free(basep_serialized_data);
    g_free(newp_serialized_data);
    if (badpixels != 0)
    {
      g_string_printf (*err_str, "- %s and %s - differ by %d pixels",
                       basefile, newfile, badpixels);
      return FALSE;
    }

    /* if we got this far there were no errors so free err_str */
    g_string_free (*err_str, TRUE);
    *err_str = NULL;
    return TRUE;
}

gchar *
gerd_snapshot_new_filename (gint playrec_type, gint snap_type, gchar *basefile)
{
    gchar *ret_str = NULL;

    int snapshot_number = 0;

    if(playrec_type)
	{
      gchar *playrec_prefix = NULL;
      gchar *curr_ptr = NULL;

      playrec_prefix = (gchar*) g_getenv("GERD_RECORD");

	  /* Get the event file name excluding the directory path name */
      while(*playrec_prefix)
      {
        curr_ptr = strchr(playrec_prefix, '/'); 

        if(curr_ptr)
        {
          playrec_prefix = ++curr_ptr;
          curr_ptr = NULL;      
        }
        else
          break;
      }

	  /* This should not come, added to handle failure case */

	  if (!playrec_prefix)
	    playrec_prefix = "temp";

	  /* Get the snapshot number */
      snapshot_number = gtt_module_read_info (GTT_REQUEST_SNAPSHOT_NUM);

      if (snapshot_number < 0)
      {
          g_message ("Client: Error in receiving the snapshot number");
          gtt_module_quit ();
      }

      ret_str = g_strdup_printf ("%s%s%02d%s", playrec_prefix, "record", snapshot_number, snap_exts[snap_type]);

	}
    else
    {
		gchar *temp_str = NULL; 
		gchar *temp_ptr = NULL;
		gchar *format_type = NULL;

		/* Get the eventfile name from the baseline to be used for 
		   naming the snapshot while playing back */

		temp_ptr = strstr(basefile, "record");

		if(temp_ptr)
		{
		  temp_str = g_malloc0 ((temp_ptr - basefile) + 1);

		  strncpy(temp_str, basefile, (temp_ptr - basefile));

		  temp_str[(temp_ptr - basefile)] = '\0';

		  temp_ptr+= strlen("record");
		}
		else
		{
	      temp_str = "temp";
		}

        ret_str = g_strdup_printf ("%s%s%s", temp_str, "play", temp_ptr); 

  		g_free(temp_str);
    }

    return ret_str;
}

void
gerd_set_baseline_dir (const gchar *dir_name)
{
  baseline_dir = g_strdup (dir_name ? dir_name : g_getenv("GERD_WORKING_DIR"));
}

void
gerd_set_snapshot_dir (const gchar *dir_name)
{
  snapshot_dir = g_strdup (dir_name ? dir_name : g_getenv("GERD_WORKING_DIR"));
}

void
gerd_snapshot_remove (gchar *filename)
{
  gchar *filepath;

  g_return_if_fail (filename != NULL);

  filepath = g_strdup_printf ("%s/%s", snapshot_dir, filename);
  unlink (filepath); 
  g_free (filepath);
}
