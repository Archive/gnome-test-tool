/* Gnome_test_tool
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "gtt-common.h"
#include "gtt-record.h"
#include "gtt-playback.h"
#include "gttconfig.h"

static void print_help(FILE *fout)
{
  fprintf (fout, "Usage: gnome-test-tool-{play|record} <eventfile> <gtkprogram> [gnome-test-tool-options] [program-options]\n");
  fprintf (fout, "Gnome-Test-Tool Options:\n");
  fprintf (fout, "  --gnome-test-tool-play=<ifile>     playback events from <ifile>\n");
  fprintf (fout, "  --gnome-test-tool-record=<ofile>   record events to <ofile>\n");
  fprintf (fout, "  --help             show this help message\n");
  fprintf (fout, "  --version          print version informations\n");
  /*fprintf (fout, "Default Features:");*/

  fprintf (fout, "Events for <gtkprogram> are either recorded to or played back from\n"); 
  fprintf (fout, "<eventfile>, depending on whether <ifile> or <ofile> got specified.\n");
  fprintf (fout, "gnome-test-tool-play and gnome-test-tool-record are convenience wrapper scripts to start "); 
  fprintf (fout, "<gtkprogram> with the Gnome-Test-Tool module.\n");
  fprintf (fout,"For more information please read the README and FAQ file\n");
}

static void
print_version (FILE *fout)
{
	static const gchar *PRG_NAME = "Gnome-Test-Tool";
	static const gchar *PKG_NAME = "Gnome-Test-Tool";
	static const gchar *PKG_HTTP_HOME = "cvs.gnome.org";

	fprintf (fout, "%s version : %s \n", PRG_NAME, GERD_VERSION);
	fprintf (fout, "Libraries: ");
	fprintf (fout, "GTK+ %u.%u.%u", gtk_major_version, gtk_minor_version, gtk_micro_version);
	fprintf (fout, ", GLib %u.%u.%u", glib_major_version, glib_minor_version, glib_micro_version);
	fprintf (fout, "\n");
	fprintf (fout, "%s comes with ABSOLUTELY NO WARRANTY.\n", PRG_NAME);
	fprintf (fout, "You may redistribute copies of %s under the terms of\n", PRG_NAME);
	fprintf (fout, "the GNU General Public License which can be found in the\n");
	fprintf (fout, "%s source package. Sources, examples and contact\n", PKG_NAME);
	fprintf (fout, "information are available at %s\n", PKG_HTTP_HOME);
}

gint main (gint argc, gchar *argv[])
{
	gint pid = -1, port;
	gchar port_env [MAX_ENV_LEN], event_pipe_env [MAX_ENV_LEN],
        seq_pipe_env[MAX_ENV_LEN], *gtt_mode=NULL;

	if (!argv[2] || !argv[3] )
	{
		g_message (" Null Input arguments ");
		exit (1);
	}

	if (!strcmp (argv[3] , "gtt-gtkdummy"))
	{
		if (!argv [4])
		{
			g_message ("Unknown Command");
			exit (0);
		}

		if (! strcmp (argv [4],  "--gnome-test-tool-version"))
		{
			print_version(stderr);
		}
		else if (! strcmp (argv [4],  "--gnome-test-tool-help"))
		{
			print_help(stderr);
		}

		exit (0);
	}

    /* Use the environmental variable GERD_PLAY to check whether it is 
	   recording or playing mode. The variable will be set in the script */

	gtt_mode = (gchar *) g_getenv ("GERD_PLAY");
	DEBUG_PLAYBACK ("GERD_PLAY environ value : %s", gtt_mode ? gtt_mode : "");

	if (gtt_mode)	/* success when playback mode */
	{
		if (!argv [1])
		{
			g_message (" Null resultant file");
			exit (1);
		}

		if ( (port = gttd_comm_init () ) == FAILURE) 
		{
			g_message ("gttd_comm_init () failed ");
			exit (1);
		}

		DEBUG_SOCK ("Port number is %d\n", port);
		sprintf (port_env, "GTT_PORT_NUMBER=%d", port);

		/* Export the port number as an environment variable to be 	
		   used by the various subprocesses */

		putenv (port_env);

	}
	else 
	{
		if (gtt_record_init (argv[2]) == FAILURE)
		{
			g_message ("gtt_record_init () failed ");
			gtt_close_pipe ();
			exit (1); 
		}

		g_message ("Started recording the events in %s ", argv[2]);	
		/* Export the named pipe names to the client application */
	
		sprintf (event_pipe_env, "GTT_EVENT_PIPE=%s",
			                  		 gtt_get_event_pipe_name ());
		putenv (event_pipe_env);

		sprintf (seq_pipe_env, "GTT_SEQ_PIPE=%s", gtt_get_seq_pipe_name ());
		putenv (seq_pipe_env);
	}

	if ( (pid = fork()) < 0 )
	{
		g_message ("Fork failed");
		exit (1);
	}

	if (!pid)
	{
		g_message ("Invoking the testing application");
		execv (argv[3], &argv[3]);
		g_message ("ERROR: Failed to exec [%s]", argv[3]);
		exit (1);
	}
	else
	{
	/* Invoke record_init or playback_init depending on the type of mode */

		if (gtt_mode) 
		{
			DEBUG_PLAYBACK ("gtt_playback called");
			gtt_playback_init (argv[2], argv[1]);
		}
		else	
		{
			DEBUG_RECORD ("gtt_record called");
			gtt_record_start ();
		}
	}

	return 0;
}
