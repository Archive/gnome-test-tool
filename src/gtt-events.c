/* Gnome_test_tool
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * This code has been derived from the GERD project by
 * Tim Janik .
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include    "gtt-common.h"
#include    "gtt-client.h" 
#include    "gtt-events.h"
#include    "gtt-io.h"
#include    "gtt-xwrapper.h"
#include    "gtt-snap.h"
#include    "gttconfig.h"

#include    <gdk/gdkkeysyms.h>
#include    <gtk/gtkinvisible.h>
#include    <stdio.h>
#include    <stdlib.h>
#include    <unistd.h>
#include    <signal.h>
#include    <X11/X.h>
#include    <X11/Xlib.h>
#include    <X11/Intrinsic.h>
#include    <X11/XWDFile.h>
#include    <X11/cursorfont.h>

#define lowbit(x) ((x) & (~(x) + 1))

#define    TIMER_OFF    ~0
#define    NEXT_PLAY_DELAY  10		
#define    RETRY_DELAY  500		/* FIXME : increase the delay to 750 */
#define    IDLE_DELAY	30000	/* 30 secs  */

/* --- prototypes --- */
static void        gerd_mask_pstate    (GdkWindow  **window,
                         gint    *win_x,
                         gint    *win_y,
                         gint    *root_x,
                         gint    *root_y,
                         guint    *mask);
static void        set_playback_timer    (guint          msecs);
static void        main_event_handler    (GdkEvent    *event,
                         gpointer     data);
static guint        play_command        (GerdCommand *cmd);
static GdkFilterReturn    root_event_monitor    (GdkXEvent   *gdk_xevent,
                         GdkEvent    *event,
                         gpointer     gdk_root);
static void        play_snapshot        (gchar       *window_id,
                         gchar       *basefile);
static gboolean        do_command        (gpointer     data);
static gboolean        gerd_check_main_level    (gpointer     data);
static void        gerd_check_version    (gchar       *script_version);

static void        gerd_subtest_set_current (gchar       *testname);

void gtt_module_quit(void);
static void gtt_module_ipc_init (gchar *win_id);
static void gtt_module_send_ack (void);
static gint gtt_module_get_next_event (gchar **str, gboolean *last_event_flag);

static gchar* get_filename (gchar *filename);

/* --- variables --- */
static gint    record_stream;
static GScanner      *playback_stream = NULL;
static guint       playback_delay = 0;
static GTimer      *event_timer = NULL;
static gulong      event_timer_stamp = 0;
static gulong      record_time_stamp = 0;
static GerdPState *masq_pstate = NULL;
static gboolean    write_delays_to_eventfile = TRUE;
static gboolean    write_warps_to_eventfile = TRUE;
static gboolean enter_flag = FALSE; /* Flag to indicate that BUTTON_PRESS 
									   has occured */ 
static gboolean do_cmd_flag = FALSE; /* Flag to call the do_command 
										explicitly */
static gchar *response_str = NULL;
static gulong no_of_events = 0;
static GttPlayBackMesgType msg_type = GTT_RESPONSE;
static FILE *fp = NULL; 

/* --- functions --- */
#include <gdk/gdkx.h>
void
gerd_events_init (void)
{
  static gboolean initialized = FALSE;

  if (!initialized)
  {
    initialized = TRUE;

    event_timer = g_timer_new ();
    gdk_event_handler_set (main_event_handler, NULL, NULL);

    gdk_root_add_watch (GDK_POINTER_MOTION_MASK, root_event_monitor, NULL);
  }
}

gulong
gerd_time_current (void)
{
  return event_timer_stamp + 1000 * g_timer_elapsed (event_timer, NULL);
}

static gulong
gerd_time_stamp_record (void)
{
  gulong cur_time;
  gulong delta;
  
  cur_time = gerd_time_current ();
  delta = cur_time - record_time_stamp;

  DEBUG ("record %lu - %lu = %lu\n", cur_time, record_time_stamp, delta);

  record_time_stamp = cur_time;

  return delta;
}

static void
gerd_time_update (gulong current)
{
  gulong rec_lap = gerd_time_stamp_record ();

  DEBUG ("stamp update %lu = %lu\n", event_timer_stamp, current);

  event_timer_stamp = current;
  record_time_stamp = event_timer_stamp - MIN (rec_lap, event_timer_stamp);
  
  g_timer_reset (event_timer);
}

void
gerd_masquerade_pstate (const GerdPState *pstate)
{
  if (pstate)
  {
    static GerdPState static_pstate;

    static_pstate = *pstate;
    masq_pstate = &static_pstate;
  }
  else
    masq_pstate = NULL;
}

gint
gerd_set_recording (const gchar *file_name,
            const gchar    *format,
            ...)
{
  gerd_stream_printf (record_stream, "(gerdversion \"%s\")\n", GERD_VERSION);
        
  if (gtt_module_get_process_count () == 0) /* Call only for  first process */
	gerd_stream_put_subtest (record_stream);

  if (write_delays_to_eventfile)
    gerd_stream_put_delay (record_stream, 1500);

  if (!write_delays_to_eventfile && playback_delay)
    gerd_stream_put_setdelay (record_stream, playback_delay);

  GERD_SET_PSTATE_WRAPPER (gerd_mask_pstate);

  return 0;
}

gint
gerd_set_playback (void)
{
  static gint sd = 0;
  playback_stream = gerd_scanner_new ();

  if (!playback_stream)
  {
    g_message ("Unable to scan the playback file");
    return GTT_IPC_FAILURE;
  }
  
  fp = fopen (get_event_file_name (), "r"); 

  if (!fp)
  {
    g_message ("Unable to open the playback file");
    return GTT_IPC_FAILURE;
  }

  if(!sd)
  {
    sd = gtt_module_connect_server();

	if (sd < 0)
	{
    	return GTT_IPC_FAILURE;
	}
  }

  GERD_SET_PSTATE_WRAPPER (gerd_mask_pstate);
  set_playback_timer (0);

  return 0;

}

void
gtt_playback_free_memory (void)
{
  DEBUG_PLAYBACK ("Inside gtt_playback_free_memory");

  if(playback_stream)
  {
    gerd_scanner_destroy (playback_stream);

    playback_stream = NULL;
    masq_pstate = NULL;
    GERD_SET_PSTATE_WRAPPER (NULL);
    set_playback_timer (TIMER_OFF);
  }
}

void 
gtt_module_quit()
{
  DEBUG_PLAYBACK ("Inside gtt_module_quit");

  if (playback_stream)
	  gtt_playback_free_memory();

  if (response_str)
  {
    g_free (response_str);
    response_str = NULL;
  }

  gtt_module_close_pipe ();
  exit(0);
}

static void
gerd_crash_handler (gint signo)
{
  struct sigaction act;
  sigset_t empty_mask;
  gchar *cmd_str;
  
  sigemptyset(&empty_mask);
  act.sa_handler = SIG_DFL;
  act.sa_mask    = empty_mask;
  act.sa_flags   = 0;
  
  cmd_str = g_strdup_printf ("echo %s > .GERD_CORE", g_getenv ("GERD_PLAY"));
  system (cmd_str);
  g_free (cmd_str);

  response_str = g_strdup_printf ("Application crashing due to %s \nCore file will be saved as  %s.core", g_strsignal(signo), g_getenv ("GERD_PLAY"));

  msg_type = GTT_CRASH_ERROR;
  gtt_module_send_ack ();

  g_free (response_str);

  if (playback_stream)
	  gtt_playback_free_memory ();

  gtt_module_close_pipe ();

  sigaction(signo, &act, 0);
}

void
gerd_set_nowait ()
{
  write_delays_to_eventfile = FALSE;
}

void
gerd_set_nowarp ()
{
  write_warps_to_eventfile = FALSE;
}

void
gerd_set_playback_delay (const gchar *delay)
{
  if (delay)
    playback_delay = (guint) atoi (delay);
}

static gboolean
gerd_grab_keyboard_pointer (GdkWindow *window)
{
  Cursor cursor;
  gint tries = 1;

  cursor = XCreateFontCursor (GDK_DISPLAY(), XC_gumby);

  while (XGrabPointer (GDK_DISPLAY (), GDK_WINDOW_XWINDOW (window), False,
                       ButtonReleaseMask, GrabModeAsync, GrabModeAsync,
                       None, cursor, CurrentTime) != GrabSuccess)
  {
    if (tries++ > 3)
      return FALSE;
  }

  tries = 1;

  while (XGrabKeyboard(GDK_DISPLAY (), GDK_WINDOW_XWINDOW (window), False,
          GrabModeAsync, GrabModeAsync, CurrentTime) != GrabSuccess)
  {
    if (tries++ > 3)
    {
      XUngrabPointer (GDK_DISPLAY (), CurrentTime);
      return FALSE;
    }
  }

  return TRUE;
}

static void
gerd_ungrab_keyboard_pointer ()
{
  XUngrabPointer (GDK_DISPLAY (), CurrentTime);
  XUngrabKeyboard (GDK_DISPLAY (), CurrentTime);
}

static GdkWindow*
gerd_find_lowest_subwindow (gint x, gint y)
{
  Window src, dest, child;

  src = dest = child = GDK_ROOT_WINDOW ();

  while (child != None)
  {
    src = dest;
    dest = child;

    XTranslateCoordinates (GDK_DISPLAY (), src, dest,
        x, y, &x, &y, &child);
  }

  return (gdk_window_lookup (dest));
}

static GdkWindow*
gerd_snapshot_get_target (GdkWindow *window, gboolean is_subwindow)
{
  XEvent event;
  GdkWindow *target_window;

  XWindowEvent (GDK_DISPLAY (), GDK_WINDOW_XWINDOW (window),
                ButtonReleaseMask, &event);

  if (event.xbutton.state & Button3Mask)
    return NULL;
  else
  {
    if (is_subwindow)
    {
      target_window = gerd_find_lowest_subwindow (event.xbutton.x_root,
                                   event.xbutton.y_root);
    }
    else
    {
      GtkWidget *toplevel = NULL;
      gchar *win_path = NULL;

      target_window = gdk_window_lookup (event.xbutton.window);
      win_path = gerd_window_create_path (target_window);
      toplevel = gerd_toplevel_from_path (win_path);
      target_window = toplevel->window;
    }

    return target_window;
  }
}

static void
main_event_handler (GdkEvent *event,
            gpointer  data)
{
  GdkWindow *window;
  GtkWidget *widget;
  guint32 event_time;
  gchar *win_id;
  gint delay_secs;
  gboolean ignore_event = FALSE;
  static gboolean need_handlers = TRUE;
  static GdkEvent *last_enter_event = NULL;
  static gboolean ipc_init_flag = FALSE;
  static gboolean window_focus_flag = TRUE;
  g_return_if_fail (event != NULL);


  if (need_handlers && playback_stream)
  {
    struct sigaction act;
    sigset_t empty_mask;
  
    sigemptyset(&empty_mask);
    act.sa_handler = gerd_crash_handler;
    act.sa_mask    = empty_mask;
    act.sa_flags   = 0;
  
    sigaction(SIGBUS,  &act, 0);
    sigaction(SIGSEGV, &act, 0);
    sigaction(SIGILL, &act, 0);
    sigaction(SIGABRT, &act, 0);
    sigaction(SIGQUIT, &act, 0);

    need_handlers = FALSE;
  }

  window = event->any.window;
  widget = gtk_get_event_widget (event);

  event_time = gdk_event_get_time (event);
  if (event_time)
    gerd_time_update (event_time);

  if (widget)
  {
    switch (event->type)
    {
      case GDK_MAP:
      if (!widget->parent)
        {
          win_id = gerd_toplevel_ensure_unique_id (widget);

  		  /* Call only first time */
		  if (!ipc_init_flag)	
		  {
		    gtt_module_ipc_init (win_id);
		    ipc_init_flag = TRUE;
		    DEBUG_SOCK_WIN_ID ("Client : unique id : %s", win_id);	
		  }
        
          if (gerd_widget_get_window_pos (widget, window) < 0)
            gerd_widget_add_window (widget, window);

          if (record_stream && GTK_IS_WINDOW (widget) &&
                GTK_WINDOW (widget)->type == GTK_WINDOW_TOPLEVEL)
          {
              GdkEvent qevent = { 0, };
              gint x, y, width, height;
    
              /* now we're mapped, need to queue a fake configure event
               * so we popup at correct position/size upon playback.
               */
             qevent.type = GDK_CONFIGURE;
             qevent.configure.window = widget->window;
             qevent.configure.send_event = FALSE;

             /* tomas - get_position doesn't take window manager decorations
                into account so use get_root_origin */

             /* gdk_window_get_position (widget->window, &x, &y); */
 
             gdk_window_get_root_origin (widget->window, &x, &y);

             gdk_drawable_get_size((GdkDrawable *)widget->window,
                                  &width, &height);        

             qevent.configure.x = x;
             qevent.configure.y = y;
             qevent.configure.width = width;
             qevent.configure.height = height;
        
             if (write_delays_to_eventfile)
               gerd_stream_put_delay (record_stream,
                                   gerd_time_stamp_record () + 150);
             gerd_stream_put_event (record_stream, &qevent);
         }
       }
       else if (gerd_widget_get_window_pos (widget, window) < 0)
         gerd_widget_add_window (widget, window);

       break;
     case GDK_CONFIGURE:
       /* we only need to trap user caused configure events
        * on GtkWindow *after* we received a GDK_MAP for it
        * need to perform gtk_window_configure_event() alike
        * checks for resize_count here
        */
       if (record_stream && widget->parent == NULL &&
           widget->window == event->configure.window &&
           gerd_widget_get_window_pos (widget, window) == 0 && 
           GTK_IS_WINDOW (widget) && 
           GTK_WINDOW (widget)->type == GTK_WINDOW_TOPLEVEL)
       {
         gint x, y, width, height;
         gdk_window_get_root_origin (widget->window, &x, &y);
         gdk_drawable_get_size((GdkDrawable *)widget->window,
                                 &width, &height);        
         event->configure.x = x;
         event->configure.y = y;
         event->configure.width = width;
         event->configure.height = height;

         if (write_delays_to_eventfile)
           gerd_stream_put_delay (record_stream, gerd_time_stamp_record ());
        
         if (write_warps_to_eventfile)
           gerd_stream_put_warp (record_stream,
                                 event->motion.x_root, event->motion.y_root);
         gerd_stream_put_event (record_stream, event);
       }
       break;
     case GDK_MOTION_NOTIFY:
       if (record_stream)
       {
         if (write_warps_to_eventfile)
         {
			delay_secs = gerd_time_stamp_record();

         	if (write_delays_to_eventfile)
            {  
              if (window_focus_flag)
                gerd_stream_put_delay (record_stream, delay_secs);
              else
               gerd_stream_put_delay (record_stream, delay_secs < MIN_WAIT_LIMIT
			  						  ? delay_secs : MIN_WAIT_LIMIT);
            }

            gerd_stream_put_event (record_stream, event);
          }
        }
      
        if (!event->any.send_event && playback_stream)
          event = NULL;
      break;
      case GDK_BUTTON_RELEASE:
        if (record_stream)
        {
          if (write_delays_to_eventfile)
            gerd_stream_put_delay (record_stream, gerd_time_stamp_record());

          if (write_warps_to_eventfile)
            gerd_stream_put_warp (record_stream, event->motion.x_root, 
                                  event->motion.y_root);

          gerd_stream_put_event (record_stream, event);
        }

        if (!event->any.send_event && playback_stream)
          event = NULL;
        break;

      case GDK_KEY_RELEASE:
        if (record_stream)
        {
          if ((event->key.keyval == GDK_F5 ||
               event->key.keyval == GDK_F6 ||
               event->key.keyval == GDK_F7 ||
               event->key.keyval == GDK_F8 ||
               event->key.keyval == GDK_F9))
          {
            ignore_event = TRUE;
          }
          else
          {
            if (write_delays_to_eventfile)
              gerd_stream_put_delay (record_stream, gerd_time_stamp_record());

            if (write_warps_to_eventfile)
              gerd_stream_put_warp (record_stream,
                                    event->motion.x_root, event->motion.y_root);

            gerd_stream_put_event (record_stream, event);
          }
        }

        if (!event->any.send_event && playback_stream)
          event = NULL;
        break;
      case GDK_BUTTON_PRESS:
      case GDK_2BUTTON_PRESS:
      case GDK_3BUTTON_PRESS:
        if (record_stream)
        {
          if (write_delays_to_eventfile)
            gerd_stream_put_delay (record_stream, gerd_time_stamp_record ());

          /* add the last ENTER_NOTIFY event if we are not recording all
             motion events so BUTTON_PRESS events work properly */
          if (!write_warps_to_eventfile && last_enter_event)
          {
            enter_flag = TRUE;
            gerd_stream_put_event (record_stream, last_enter_event);
          }

          if (write_warps_to_eventfile)
            gerd_stream_put_warp (record_stream, event->button.x_root,
                                  event->button.y_root);

          gerd_stream_put_event (record_stream, event);
        }

        if (!event->any.send_event && playback_stream)
          event = NULL;
      break;
      case GDK_KEY_PRESS:
        if (record_stream)
        {
          switch (event->key.keyval)
          {
            case GDK_F5:
            case GDK_F6:
            {
              GdkWindow *snap_win = NULL;
              gchar *filename = NULL;
              gchar *format_type = NULL;
              gboolean is_subwindow = (event->key.keyval == GDK_F6);

	      format_type = g_strdup(g_getenv("GERD_IMAGE_FORMAT"));
	      if (format_type == NULL)
		format_type = g_strdup("gtt");

              if (gerd_grab_keyboard_pointer(window))
              {
                snap_win = gerd_snapshot_get_target (window, is_subwindow);
                gerd_ungrab_keyboard_pointer();
              }

              if (snap_win)
              {
		  if (strstr(format_type, "gtt")) {
                   filename = gerd_snapshot_new_filename (GERD_SNAP_REC,
						GERD_SNAP_IMAGE_GTT, NULL);
                   gerd_snapshot_gtt_get (snap_win, filename);
		  } else {
                   filename = gerd_snapshot_new_filename (GERD_SNAP_REC,
						GERD_SNAP_IMAGE_PNG, NULL);
                   gerd_snapshot_png_get (snap_win, filename);
		  }

                  g_message ("Bitmap snapshot of %s saved to %s\n",
                              is_subwindow ? "widget" : "window", filename);  

                  if (write_delays_to_eventfile)
                    gerd_stream_put_delay (record_stream,
                                   gerd_time_stamp_record ());

                  if (write_warps_to_eventfile)
                    gerd_stream_put_warp (record_stream, event->motion.x_root, 
                                          event->motion.y_root);
            
                  gerd_stream_put_snap (record_stream, snap_win, filename);

                  g_free (filename);
		  if (format_type)
		   g_free(format_type);
              }
          
              ignore_event = TRUE;
            }
            break;
            case GDK_F7:
            case GDK_F8:
            {
              GdkWindow *snap_win = NULL;
              gchar *filename = NULL;
              gboolean is_subwindow = (event->key.keyval == GDK_F8);

              if (gerd_grab_keyboard_pointer(window))
              {
                snap_win = gerd_snapshot_get_target (window, is_subwindow);
                gerd_ungrab_keyboard_pointer();
              }

              if (snap_win)
              {
                filename = gerd_snapshot_new_filename (GERD_SNAP_REC,
                                            GERD_SNAP_RESOURCES, NULL);
                gerd_snapshot_snp_get (snap_win, filename);
        
		  		g_message ("Object snapshot of %s saved to %s\n", 
				            is_subwindow ?  "widget" : "window", filename); 
                gerd_stream_put_snap (record_stream, snap_win, filename);
              }

              ignore_event = TRUE;
            }
            break;
            case GDK_F9:
              gerd_stream_put_subtest (record_stream);
              ignore_event = TRUE;
              break;
            default:
            if (write_delays_to_eventfile)
              gerd_stream_put_delay (record_stream,
                                     gerd_time_stamp_record ());

            gerd_stream_put_event (record_stream, event);
            break;
          }
        }

        if (!event->any.send_event && playback_stream)
          event = NULL;
        break;
        case GDK_ENTER_NOTIFY:
        case GDK_LEAVE_NOTIFY:
        if (record_stream)
        {
          if (write_warps_to_eventfile) 
          {
			delay_secs = gerd_time_stamp_record();

         	if (write_delays_to_eventfile)
            {
         	  if (window_focus_flag)
                gerd_stream_put_delay (record_stream, delay_secs);
              else
               gerd_stream_put_delay (record_stream, delay_secs < MIN_WAIT_LIMIT
	  								  ? delay_secs : MIN_WAIT_LIMIT);
            }

            gerd_stream_put_warp (record_stream,
                           event->crossing.x_root, event->crossing.y_root);
            
			gerd_stream_put_event (record_stream, event);
          }
          else 
          {
			delay_secs = gerd_time_stamp_record();

         	if (write_delays_to_eventfile)
            {
              if (window_focus_flag)
                gerd_stream_put_delay (record_stream, delay_secs);
              else
               gerd_stream_put_delay (record_stream, delay_secs < MIN_WAIT_LIMIT
									  ? delay_secs : MIN_WAIT_LIMIT);
            }

            if (event->type == GDK_ENTER_NOTIFY)
            {
              if (last_enter_event)
                gdk_event_free (last_enter_event);

              last_enter_event = gdk_event_copy (event);
            }
            else
            {
              if(enter_flag == TRUE)
              {
                enter_flag = FALSE;
                gerd_stream_put_event (record_stream, event);
              } 
            }
          }
        }
        if (playback_stream)
        event = NULL;
        break;
      case GDK_DELETE:
        if (record_stream)
        {
          if (write_delays_to_eventfile)
            gerd_stream_put_delay (record_stream, gerd_time_stamp_record ());

          gerd_stream_put_event (record_stream, event);
        }
        if (playback_stream)
        {
          g_message ("Received GDK_DELETE ");
          gtt_module_quit ();
        }
        break;
      case GDK_DESTROY:
		/* FIXME: isn't triggered */
        gerd_widget_remove_window (widget, window);
        break;
      case GDK_FOCUS_CHANGE:
        if (record_stream)
		{
			if(event->focus_change.in)
			{
		 	  /* Avoid unnecessary delay */
			  delay_secs = gerd_time_stamp_record();

         	  if (write_delays_to_eventfile )
              {
                if (window_focus_flag)
                  gerd_stream_put_delay (record_stream, delay_secs);
                else
                 gerd_stream_put_delay (record_stream, 
                    delay_secs < MIN_WAIT_LIMIT ? delay_secs : MIN_WAIT_LIMIT);
              }
		
        	  gerd_stream_put_event (record_stream, event);
		  	  window_focus_flag = TRUE;
        	}
   			else
        	{
            	DEBUG_PERFORMANCE ("Client : Inside focus lost flag");
            	window_focus_flag = FALSE;
        	}
   		} 
        break;

      default:
        break;
    }
  }
  
  if (event && ignore_event == FALSE)
  {
    if (masq_pstate)
        gdk_event_set_state (event, masq_pstate->mod_mask);
      
    gtk_main_do_event (event);
  }
}

static void
play_event (GdkEvent *event)
{
  GtkWidget *widget;

  g_return_if_fail (event != NULL);

  widget = gtk_get_event_widget (event);
  switch (event->type)
  {
    case GDK_CONFIGURE:
      DEBUG ("PLAY: Configure Event\n");
      gdk_window_move_resize (event->configure.window,
                  event->configure.x, event->configure.y,
                  event->configure.width, event->configure.height);
      break;
    case GDK_MOTION_NOTIFY:
      DEBUG ("PLAY: Motion Event\n");
      gdk_root_warp_pointer (event->motion.x_root, event->motion.y_root);
      gdk_flush ();
      if (GTK_IS_WINDOW (widget))
          gdk_window_raise (widget->window);
      while (gdk_events_pending ())
        g_main_context_iteration(NULL, FALSE);
      event->motion.time = gerd_time_current ();
      gtk_main_do_event (event);
      break;
    case GDK_BUTTON_PRESS:
    case GDK_2BUTTON_PRESS:
    case GDK_3BUTTON_PRESS:
    case GDK_BUTTON_RELEASE:
      DEBUG ("PLAY: Button Event\n");
      if (GTK_IS_WINDOW (widget))
        gdk_window_raise (widget->window);
      while (gdk_events_pending ())
        g_main_context_iteration(NULL, FALSE);
      event->button.time = gerd_time_current ();
      gtk_main_do_event (event);
      break;
    case GDK_ENTER_NOTIFY:
    case GDK_LEAVE_NOTIFY:
      DEBUG ("PLAY: Crossing Event\n");
      while (gdk_events_pending ())
        g_main_context_iteration(NULL, FALSE);
      event->crossing.time = gerd_time_current ();
      gtk_main_do_event (event);
      break;
    case GDK_KEY_PRESS:
    case GDK_KEY_RELEASE:
      DEBUG ("PLAY: Key Event\n");
      if (GTK_IS_WINDOW (widget))
        gdk_window_raise (widget->window);
      gdk_flush ();
      while (gdk_events_pending ())
        g_main_context_iteration(NULL, FALSE);
      event->key.time = gerd_time_current ();
      gtk_main_do_event (event);
      break;
    case GDK_DELETE:
      DEBUG ("PLAY: Delete Window\n");
      gtk_main_do_event (event);
      break;
    case GDK_FOCUS_CHANGE:
      DEBUG ("PLAY: Focus Change Event\n");
      gtk_main_do_event (event);
      gdk_window_focus(event->any.window, gerd_time_current ());
      if (GTK_IS_WINDOW (widget))
        gdk_window_raise (widget->window);
      break;
    default:
      g_warning (G_STRLOC ": cannot play event type %d", event->type);
      break;
  }
}

static guint
play_command (GerdCommand *cmd)
{
  guint msecs = 0;

  g_return_val_if_fail (cmd != NULL, TIMER_OFF);

  switch (cmd->cmd)
  {
    case GERD_CMD_DELAY:
      msecs = cmd->data.delay;
      DEBUG ("PLAY: delay %u\n", msecs);
      break;
    case GERD_CMD_EVENT:
      gtk_idle_add (gerd_check_main_level, GINT_TO_POINTER (gtk_main_level ()));

    /* 
	 * If the play_event doesn't returns within the IDLE_DELAY time, it is 
     * assumed that the play_event goes into infinite loop. To avoid this 
     * situation we are calling the do_command () every IDLE_DELAY. Suppose
     * the do_command () returns within the IDLE_DELAY, then this call will
     * be ignored. 
     */   	

      set_playback_timer (IDLE_DELAY);		
      play_event (cmd->data.event);
      break;
    case GERD_CMD_SNAPSHOT:
      DEBUG ("PLAY: Take a snaphost\n");
      play_snapshot (cmd->window_id, cmd->data.filename);
      break;
    case GERD_CMD_VERSION:
      DEBUG ("PLAY: Check gerd version number of script\n");
      gerd_check_version (cmd->data.version);
      break;
    case GERD_CMD_SUBTEST:
      DEBUG ("PLAY: SubTest\n");
      gerd_subtest_set_current (cmd->data.testname);
      break;
    case GERD_CMD_SETDELAY:
      DEBUG ("PLAY: Set playback delay\n");
      playback_delay = cmd->data.playback_delay;
      break;
    case GERD_CMD_WARP:
      DEBUG ("PLAY: Warp\n");
      gdk_root_warp_pointer (cmd->data.warp.root_x, cmd->data.warp.root_y);
      break;
    case GERD_CMD_PSTATE:
      DEBUG ("PLAY: Pointer State\n");
      gerd_masquerade_pstate (&cmd->data.pstate);
      break;
    case GERD_CMD_EOF:
      DEBUG ("PLAY: end of file\n");
      gtt_module_quit ();
      msecs = TIMER_OFF;
      break;
    case GERD_CMD_LAST:
      /* ... */
      break;
    case GERD_CMD_APP_ID:
      msecs = 5; 
      break;
  }

  return msecs;
}

/* function to check if the event-file we are reading was recorded with
   the same version of Gnome-Test-Tool we are using */
static void
gerd_check_version (gchar *script_version)
{
  if (g_ascii_strcasecmp (script_version, GERD_VERSION) != 0)
  {
    /*g_message ("Script version [%s] incompatible with GERD lib version [%s]\n", script_version, GERD_VERSION);*/

   /* msg_type = GTT_VERSION_ERROR;*/

    response_str = g_strdup_printf ("Script version [%s] incompatible with GTT lib version [%s] \n ", script_version, GERD_VERSION); 

  }
}

/* function to check if we are in the top level main loop */
static gboolean
gerd_check_main_level (gpointer data)
{
  gint toplevel = GPOINTER_TO_INT (data);

  if ((gtk_main_level () != toplevel) || (do_cmd_flag == TRUE))
    do_command (NULL);

  return FALSE;
}

static gboolean
do_command (gpointer data)
{
  GerdCommand *cmd;
  static gchar *str = NULL;
  static gboolean retry_flag = FALSE;
  gboolean last_event_flag = FALSE;
  gulong local_count = 0;

  set_playback_timer (TIMER_OFF);    

  if (gtk_events_pending ())
    gtk_main_iteration_do (FALSE);
 
  local_count = ++no_of_events;	

  if (retry_flag == FALSE)
  {
    if (do_cmd_flag) 
    {
      DEBUG_PLAYBACK ("do_cmd flag is true, send ack");		
      gtt_module_send_ack ();
    }
    
    msg_type = GTT_RESPONSE;

    gtt_module_get_next_event (&str, &last_event_flag);

    if (last_event_flag)
    {
	    DEBUG_READ_IO ("last_event_flag true");	
	    msg_type = GTT_LAST_EVENT;	
      gtt_module_send_ack ();
      set_playback_timer (NEXT_PLAY_DELAY);
	  
	    return FALSE; 
    }

  }

  /* Prepares to scan the received text buffer(event) */
  g_scanner_input_text (playback_stream, str, strlen(str)); 

  cmd = gerd_peek_command (playback_stream);
  if (cmd && gerd_complete_command (playback_stream, cmd))
  {
    gint msecs = -1;
    gint wait_secs = -1;

    cmd = gerd_get_next_command (playback_stream);

    do_cmd_flag = TRUE; /* set the flag */

    msecs = play_command (cmd);
   
    do_cmd_flag = FALSE; /* reset the flag */
      
    g_free (str);
    str = NULL;
    retry_flag = FALSE;
    gerd_free_command (cmd);
    
    if (msecs != 0)
      wait_secs = gerd_time_stamp_record();
    else
      wait_secs =0 ;
    msecs = msecs - wait_secs;
    if (msecs < 0)
      msecs = 0;
    
    if (no_of_events == local_count)	
    {
      gtt_module_send_ack ();
      set_playback_timer (msecs ? msecs : playback_delay);
    }
  }
  else if (cmd)
  {
    static guint failures = 0;

    DEBUG ("RETRY, failed to lookup window: %s\n", cmd->window_id);

    retry_flag = TRUE;

    if (cmd->retries >= 2)
    {
      retry_flag = FALSE;

      g_free (str);
      str = NULL;

      if (cmd->cmd == GERD_CMD_EVENT)
      {
        msg_type = GTT_CROSSING_SUBWINDOW_ERROR;
        response_str = g_strdup_printf ("%s%s", "failed to find window: ", 
                                                 cmd->window_id);

        if ((cmd->data.event->type == GDK_BUTTON_PRESS ||
             cmd->data.event->type == GDK_2BUTTON_PRESS ||
             cmd->data.event->type == GDK_3BUTTON_PRESS ||
             cmd->data.event->type == GDK_BUTTON_RELEASE ||
             cmd->data.event->type == GDK_KEY_PRESS ||
             cmd->data.event->type == GDK_KEY_RELEASE))
        {
          DEBUG ("SKIPPING COMMAND, too many retries\n");

          failures++;
      
          if (failures >= 7)
          {
            g_message ("ABORTING, too many failures\n");
            msg_type = GTT_RESPONSE_ERROR; 
            gtt_module_send_ack ();
          }
        }
      }

      cmd = gerd_get_next_command (playback_stream);
      gerd_free_command (cmd);
    }
    
    if (msg_type != GTT_RESPONSE_ERROR)
      set_playback_timer (RETRY_DELAY);
  }
  else
  {
      DEBUG_PLAYBACK ("Received improper command ");

      g_free (str);

      retry_flag = FALSE;

      msg_type = GTT_RESPONSE_ERROR;
      response_str = g_strdup ("Received improper command");
      gtt_module_send_ack ();
  }

  g_free (response_str);
  response_str = NULL;

  return FALSE;
}

static GdkFilterReturn
root_event_monitor (GdkXEvent *gdk_xevent,
            GdkEvent  *event,
            gpointer   gdk_root)
{

/*
  XEvent *xevent = gdk_xevent;

  if (record_stream && xevent->type == MotionNotify)
  {
    if (write_warps_to_eventfile)
    {
      if (write_delays_to_eventfile)
        gerd_stream_put_delay (record_stream, gerd_time_stamp_record ());

      gerd_stream_put_warp (record_stream,
                 xevent->xmotion.x_root, xevent->xmotion.y_root);
    }
  }
*/  /* No need to record unnecessary mouse move events occured in other 
       than testing application area */

  return GDK_FILTER_CONTINUE;
}

static void
gerd_mask_pstate (GdkWindow **window,
          gint       *win_x,
          gint       *win_y,
          gint       *root_x,
          gint       *root_y,
          guint      *mask)
{

  /* FIXME -- pstate is not working currently, it is not invoked */

  if (record_stream)
    gerd_stream_put_pstate (record_stream, *window, *win_x, *win_y, *mask);

  if (playback_stream && masq_pstate)
  {
    GerdCommand *cmd = gerd_peek_command (playback_stream);

    if (cmd && cmd->cmd == GERD_CMD_PSTATE)
        do_command (NULL);
  
    *window = masq_pstate->window;
    *win_x = masq_pstate->win_x;
    *win_y = masq_pstate->win_y;
    *mask = masq_pstate->mod_mask;

    *root_x = *win_x;
    *root_y = *win_y;
    gdk_window_translate (*window, NULL, root_x, root_y);
  }
}

static void
set_playback_timer (guint msecs)
{
  static guint marshal_timer_id = 0;

  if (marshal_timer_id)
  {
    g_source_remove (marshal_timer_id);
    marshal_timer_id = 0;
  }

  if (playback_stream && msecs != TIMER_OFF)
    marshal_timer_id = g_timeout_add_full (G_PRIORITY_DEFAULT,
                                msecs,
                                do_command,
                                NULL, NULL);
}

static gchar*
get_filename (gchar *filename)
{
  gchar *temp_ptr = NULL;
  gchar *temp_str = NULL;
  gchar *ret_str = NULL;

  /*  Get the name of the diff file to be used for storing the
      difference in the objet snapshot while playing back */

  temp_ptr = strstr (filename, ".snp");

  if(temp_ptr)
  {
    temp_str = g_malloc0 ((temp_ptr - filename) + 1);

    strncpy(temp_str, filename, (temp_ptr - filename));

    temp_str[(temp_ptr - filename)] = '\0';
  }
  else
  {
    temp_str = "temp";
  }

  ret_str = g_strdup_printf ("%s/%s%s", g_getenv ("GERD_SNAPSHOT_DIR"), 
                             temp_str, ".diff");

  if (temp_ptr)
    g_free (temp_str);

  return ret_str;
}

static void
play_snapshot (gchar *window_id, gchar *basefile)
{
  GdkWindow *window;
  GString *err_str = NULL;
  gchar *newfile;

  gboolean err_flag = FALSE;

  while (gtk_events_pending ())
    gtk_main_iteration_do (FALSE);

  window = gerd_window_from_path (window_id);

  if (strstr (basefile, "png"))
  {
    newfile = gerd_snapshot_new_filename (GERD_SNAP_PLAY, GERD_SNAP_IMAGE_PNG, basefile);
    if (newfile == NULL) {
      msg_type = GTT_SNAPSHOT_ERROR;
      g_message("Basefile %s is no present in the system, cannot compare results\n", basefile);
     return;
    }
    gerd_snapshot_png_get (window, newfile);

    if (gerd_snapshot_png_compare (basefile, newfile, &err_str))
    {
      gerd_snapshot_remove (newfile);

      msg_type = GTT_SNAPSHOT_PASS;

      response_str = g_strdup_printf ("%s%s%s%s%s", "Image snapshot comparison of ", basefile, " and " , newfile , " passed");

      g_message  ("Comparing %s and %s ... PASSED\n", basefile, newfile);
    }
    else
    {
      msg_type = GTT_SNAPSHOT_FAIL;

      response_str = g_strdup_printf ("%s%s%s%s%s", "Image snapshots ", basefile, " and " , newfile , " differ");

      g_message ("Comparing %s and %s ... FAILED\n", basefile, newfile);
    }

    g_free (newfile);
  }

  else if (strstr(basefile,"gtt")) {
    newfile = gerd_snapshot_new_filename (GERD_SNAP_PLAY, 
						GERD_SNAP_IMAGE_GTT, 
						basefile);
    gerd_snapshot_gtt_get (window, newfile);

    if (gerd_snapshot_gtt_compare (basefile, newfile, &err_str))
    {
      gerd_snapshot_remove (newfile); 

      msg_type = GTT_SNAPSHOT_PASS;

      response_str = g_strdup_printf ("%s%s%s%s%s", "Image snapshot comparison of ", basefile, " and " , newfile , " passed");

      g_message  ("Comparing %s and %s ... PASSED\n", basefile, newfile);
    }
    else
    {
      msg_type = GTT_SNAPSHOT_FAIL;

      response_str = g_strdup_printf ("%s%s%s%s%s", "Image snapshots ", basefile, " and " , newfile , " differ");

      g_message ("Comparing %s and %s ... FAILED\n", basefile, newfile);
    }

    g_free (newfile);
  }
  else
  {
    gint result;

    newfile = gerd_snapshot_new_filename (GERD_SNAP_PLAY, GERD_SNAP_RESOURCES, basefile);
    gerd_snapshot_snp_get (window, newfile);

    if ((result = gerd_snapshot_snp_compare (basefile, newfile, &err_str)) == -1)
    {
      msg_type = GTT_SNAPSHOT_FAIL;

      response_str = g_strdup_printf ("%s", "Couldn't run diff command ");

      g_message  ("Comparing %s and %s ... FAILED\n", basefile, newfile);
    }
    else if (result == 0)
    {
      gerd_snapshot_remove (newfile);

      msg_type = GTT_SNAPSHOT_PASS;

      response_str = g_strdup_printf ("%s%s%s%s%s", "Object snapshot comparison of ", basefile, " and " , newfile , " passed");

      g_message  ("Comparing %s and %s ... PASSED\n", basefile, newfile);
    }
    else
    {
      msg_type = GTT_SNAPSHOT_FAIL;

      response_str = g_strdup_printf ("%s%s%s%s%s", "Object snapshots ", basefile, " and " , newfile , " differ");

      err_flag = TRUE;

      g_message ("Comparing %s and %s ... FAILED\n", basefile, newfile);
    }

    if (!err_flag)
    	g_free (newfile);
  }

  if (err_str)
  {
    gchar *temp_str = NULL;
    gchar *filename = NULL;
    FILE *fptr = NULL;

    if (err_flag)
    {	
      filename = get_filename (newfile);

      fptr = fopen (filename, "w");

      if (!fptr)
      {
        g_message ("Unable to open the file for writing the differences of snapshots" );

        temp_str = g_strdup_printf ("%s\n%s", response_str, "Unable to open the  file for writing the differences of snapshots");

      }
      else
      {
        fputs (err_str->str, fptr);

        fclose (fptr);

        temp_str = g_strdup_printf ("%s\n%s%s%s", response_str, "See the difference in ", filename, " file");
      }

       g_free (filename);
       g_free (newfile);
    }
    else
    {
      temp_str = g_strdup_printf ("%s\n\t%s", response_str, err_str->str);
    }

	g_free (response_str);

    msg_type = GTT_SNAPSHOT_ERROR;
	response_str = temp_str;

    g_string_free (err_str, TRUE);
  }
}

static void
gerd_subtest_set_current (gchar *testname)
{
  g_message  ("Starting subtest %s\n", testname);

  msg_type = GTT_SUBTEST;
  response_str = g_strdup (testname);
}

static void
gtt_module_ipc_init (gchar *win_id)
{
  gchar str[1024];
  
  sprintf (str, "(app_id \"%s\")", win_id); 	
  DEBUG_READ_IO ("client : %s", str);

  if (is_playback_mode ())
  {
    if ( gerd_set_playback () == GTT_IPC_FAILURE)
    {
      g_message  ("Unable to connect to server");
      gtt_module_quit ();
    }

    gtt_module_send_result (GTT_WIN_ID, str); /* send win_id to server */
  }
  else if (is_record_mode ())
  {
    /* Initialize the IPC for writing the events */
    record_stream = gtt_module_record_init (); 

    if (record_stream == GTT_IPC_FAILURE)
    {
      g_message  ("Unable to open pipe for writing/reading the events");
      gtt_module_quit ();
    }
	
    gerd_stream_printf (record_stream,"%s\n", str);
    gerd_set_recording (NULL, "# Gerd event stream (%s)\n\n", g_get_prgname ());
  }
  else
  {
    g_message ("Failed to load gtt module, Please use gnome-test-tool-record/play command");
    gtt_module_quit (); 
  }
}

static void 
gtt_module_send_ack (void)
{
  
  if (msg_type == GTT_RESPONSE && !response_str)
    return ;

  if (!response_str)
      response_str = g_strdup ("Played"); 

  if (gtt_module_send_result(msg_type, response_str) == GTT_IPC_FAILURE)
  {
    g_message ("Failed to send the message type : %d ", msg_type);
    gtt_module_quit ();
  }

  if (msg_type == GTT_RESPONSE_ERROR || msg_type == GTT_VERSION_ERROR)
  {
      gtt_module_quit ();
  }

}

static gint 
gtt_module_get_next_event (gchar **str, gboolean *last_event_flag)
{
  gint ret_val;
  static gint tag = -1;
  static gint prev_tag = -1;
  static glong fpos = -1;
  gchar event[MAX_EVENT_SIZE];

  /*g_message ("Inside get_next_event");*/

  if (fpos == -1)
  {  
    /* Receive the file position to be played back */		  
    do 
    {
       ret_val = gtt_module_receive_event (&fpos);
      
       while (gtk_events_pending ())
         gtk_main_iteration_do (FALSE);

    } while (ret_val == GTT_IPC_EAGAIN); 
	
    if (ret_val == GTT_IPC_FAILURE)
    {
      g_message  ("Client : Unable to receive event for playback");
      gtt_module_quit();
    }

  	/*g_message ("Client : fpos values is %ld", fpos);*/

	  if (fseek (fp, fpos, SEEK_SET) == -1)
    {
      g_message  ("Client : Invalid file position received");
      gtt_module_quit();
    }

	  fscanf (fp, "%d %[^\n]", &tag, event); 
    prev_tag = tag;
  }
  else
  {	
    prev_tag = tag;

    if	(fscanf (fp, "%d %[^\n]", &tag, event) == EOF)
    {
	    tag = -1;
	    *str = NULL;
	    DEBUG_READ_IO ("Reached End of file"); 
	  }
  }

  *last_event_flag = (tag == prev_tag) ? FALSE : TRUE;

  if (*last_event_flag)
    fpos = -1;

  *str = g_strdup (event);

  return TRUE;
}
