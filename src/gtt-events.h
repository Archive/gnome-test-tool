/* Gnome_test_tool
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * This code has been derived from the GERD project by
 * Tim Janik .
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GERD_EVENTS_H__
#define __GERD_EVENTS_H__

#include	<src/gtt-main.h>
#include	"gtt-client.h" 

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */


/* --- enum --- */
typedef enum
{
  GTT_ACKNOWLEGMENT,
  GTT_ERROR_MSG,
  GTT_NORMAL_MSG,
  GTT_FINAL_RESULT
} Gtt_Event_Status;

/* --- structures --- */
struct _GerdPState
{
  GdkWindow *window;
  gint	     win_x;
  gint	     win_y;
  guint	     mod_mask;
};

/* --- prototypes --- */
void	gerd_events_init	 (void);
gulong	gerd_time_current	 (void);
gint	gerd_set_playback	 (void);
void	gerd_set_nowait		 (void);
void	gerd_set_nowarp		 (void);
void	gerd_set_playback_delay	 (const gchar *delay);
gint	gerd_set_recording	 (const gchar	  *file_name,
				  const gchar	  *format,
				  ...);
void	gerd_masquerade_pstate	 (const GerdPState *pstate);

void gtt_playback_free_memory(void);
void gtt_module_quit(void);

#ifdef __cplusplus
#pragma {
}
#endif /* __cplusplus */

#endif /* __GERD_EVENTS_H__ */
