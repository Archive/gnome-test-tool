
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <gtk/gtk.h>
#include <gdk-pixbuf/gdk-pixdata.h>

void destroy( GtkWidget *widget,
              gpointer   data )
{
    gtk_main_quit ();
}

int main( int   argc,
          char *argv[] )
{
    static GtkWidget *window;
    GtkWidget *scrolled_window;
    GtkWidget *table;
    GtkWidget *button,*image;
    char buffer[32];
    int i, j;
    GdkPixbuf *buf;
    char *path;  

    GdkPixdata pixdata;
    FILE *fp;
    GError **err;
    guint8 *mystruct;
    struct stat mybuf;

    if (!argv[1]) {
     g_message("Null Input arguments ");
     exit(1);
    }

    path = argv[1];
    printf("path is %s\n", argv[1]);
    gtk_init (&argc, &argv);
    
    /* Create a new dialog window for the scrolled window to be
     * packed into.  */
    window = gtk_dialog_new ();
    g_signal_connect (G_OBJECT (window), "destroy",
		      G_CALLBACK (destroy), NULL);
    gtk_window_set_title (GTK_WINDOW (window), "GTT Image File Viewer");
    gtk_container_set_border_width (GTK_CONTAINER (window), 0);
    gtk_widget_set_size_request (window, 300, 300);
    
    /* create a new scrolled window. */
    scrolled_window = gtk_scrolled_window_new (NULL, NULL);
    
    gtk_container_set_border_width (GTK_CONTAINER (scrolled_window), 10);
    
    /* the policy is one of GTK_POLICY AUTOMATIC, or GTK_POLICY_ALWAYS.
     * GTK_POLICY_AUTOMATIC will automatically decide whether you need
     * scrollbars, whereas GTK_POLICY_ALWAYS will always leave the scrollbars
     * there.  The first one is the horizontal scrollbar, the second, 
     * the vertical. */
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window),
                                    GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);
    /* The dialog window is created with a vbox packed into it. */								
    gtk_box_pack_start (GTK_BOX (GTK_DIALOG(window)->vbox), scrolled_window, 
			TRUE, TRUE, 0);
    gtk_widget_show (scrolled_window);
    
    /* create a table of 10 by 10 squares. */
    table = gtk_table_new (10, 10, FALSE);
    
    /* set the spacing to 10 on x and 10 on y */
    gtk_table_set_row_spacings (GTK_TABLE (table), 10);
    gtk_table_set_col_spacings (GTK_TABLE (table), 10);
    
    /* pack the table into the scrolled window */
    gtk_scrolled_window_add_with_viewport (
                   GTK_SCROLLED_WINDOW (scrolled_window), table);
    gtk_widget_show (table);
    
    /* this simply creates a grid of toggle buttons on the table
     * to demonstrate the scrolled window. */

    fp = fopen(path,"rb");
    if ( fp == NULL) {
     g_message("gtt-viewer : File %s does not exists ", path);
     exit(1);	
    }
    stat(path, &mybuf);
    mystruct = (guint8 *)malloc(mybuf.st_size);
    fread(mystruct,sizeof(guint8), mybuf.st_size,fp);
    if (!gdk_pixdata_deserialize(&pixdata, mybuf.st_size, mystruct,NULL)) {
	 g_message ("***** => pixdata screwed up ");
         exit(1);
	}
    buf = gdk_pixbuf_from_pixdata(&pixdata, FALSE, NULL);	

    image = gtk_image_new_from_pixbuf (buf);
    gtk_table_attach_defaults (GTK_TABLE (table), image,
	                             0, 1, 0, 1);
    gtk_widget_show (image);
    
    /* Add a "close" button to the bottom of the dialog */
    button = gtk_button_new_with_label ("close");
    g_signal_connect_swapped (G_OBJECT (button), "clicked",
			      G_CALLBACK (gtk_widget_destroy),
			      G_OBJECT (window));
    
    /* this makes it so the button is the default. */
    
    gtk_box_pack_start (GTK_BOX (GTK_DIALOG (window)->action_area), button, TRUE, TRUE, 0);
    
    gtk_widget_show (button);
    gtk_widget_show (window);
    
    gtk_main();
    
    return 0;
}
