/* Gnome_test_tool
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GTT_RESULT_H__
#define __GTT_RESULT_H__

/* --- prototypes --- */
gint gtt_update_results (GttPlayBackMesgFormat *playback_mesg, gpointer stream);
void gtt_playback_stream_printf (gpointer stream, const gchar *format, ...);
void gtt_dump_results (gpointer stream);
void gtt_set_curtest_result (gint result);


#endif /* __GTT_RESULT_H__ */
