/* Gnome_test_tool
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * This code has been derived from the GERD project by
 * Tim Janik .
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include	"gtt-common.h"
#include	"gtt-main.h"
#include	"gtt-events.h"
#include	"gtt-snap.h"
#include	<gmodule.h>
#include	<string.h>
#include	<stdio.h>
#include	<stdlib.h>

/* --- prototypes --- */
static void parse_args			(gint    	*argc_p,
					 gchar	      ***argv_p);
static void print_blurb			(FILE		*fout,
					 gboolean	 print_help);

void gtt_main_quit(void);

/* --- variables --- */
static GModule  *g_module = NULL;
static gchar    *gerd_ifile = NULL;
static gchar    *gerd_ofile = NULL;


/* --- functions --- */
static void
gerd_init (gint    *argc_p,
	   gchar ***argv_p)
{
  parse_args (argc_p, argv_p);

  gerd_events_init ();
  
  if (atexit (gtt_main_quit) != 0)
  {
    g_message ("Failed to register gtt_main_quit function");
  }
 
}

gboolean
is_record_mode (void)
{
  return gerd_ofile ? TRUE : FALSE;
}

gboolean
is_playback_mode (void)
{
  return gerd_ifile ? TRUE : FALSE;
}

gchar *
get_event_file_name (void)
{
  return gerd_ofile ? gerd_ofile : gerd_ifile;
}

void
gtt_main_quit (void)
{
	if (gerd_ifile)
	{
		if (gtt_module_send_result (GTT_EXIT, "Exit") == GTT_IPC_FAILURE)
		{
			DEBUG_PLAYBACK ("Unable to send the Exit event");	
		}

    gtt_playback_free_memory (); 
	}
	else if (gerd_ofile)
	{
		if (gtt_module_record_event ("Exit", GTT_MODULE_QUIT) == GTT_IPC_FAILURE)
		{
			DEBUG_RECORD ("Unable to send the Quit event");
		}
	}

	gtt_module_close_pipe ();
}

static GDebugKey gerd_debug_keys[] = {
  { "verbose",		GERD_FEAT_VERBOSE, },
  { "force-exit",	GERD_FEAT_FORCE_EXIT, },
};
static const guint gerd_n_debug_keys = sizeof (gerd_debug_keys) / sizeof (gerd_debug_keys[0]);
GerdDebugFlags   gerd_debug_flags = 0;

static void
parse_args (gint    *argc_p,
	    gchar ***argv_p)
{
  guint argc = *argc_p;
  gchar **argv = *argv_p;
  gchar *prg_name = "gtt";
  guint prg_name_length = strlen (prg_name);
  const gchar *envar;
  guint i, e;
  
  envar = g_getenv ("GERD_ENABLE");
  if (envar)
    gerd_debug_flags |= g_parse_debug_string (envar, gerd_debug_keys, gerd_n_debug_keys);
  envar = g_getenv ("GED_DISABLE");
  if (envar)
    gerd_debug_flags &= ~g_parse_debug_string (envar, gerd_debug_keys, gerd_n_debug_keys);
  gerd_ifile = g_strdup (g_getenv ("GERD_PLAY"));
  gerd_ofile = g_strdup (g_getenv ("GERD_RECORD"));

  gerd_set_baseline_dir (g_getenv ("GERD_BASELINE_DIR"));
  gerd_set_snapshot_dir (g_getenv ("GERD_SNAPSHOT_DIR"));
  gerd_set_playback_delay (g_getenv ("GERD_PLAYBACK_DELAY"));
  if (g_getenv ("GERD_NOWAIT")) gerd_set_nowait();
  if (g_getenv ("GERD_NOWARP")) gerd_set_nowarp();

  for (i = 1; i < argc; i++)
  {
    if (strncmp (argv[i], "--", 2) == 0 &&
        strncmp (argv[i] + 2, prg_name, prg_name_length) == 0 &&
	    strncmp (argv[i] + 2 + prg_name_length, "-enable", 7) == 0 &&
	    (argv[i][2 + prg_name_length + 7] == 0 || argv[i][2 + prg_name_length + 7] == '='))
	{
	  gchar *equal = argv[i] + 2 + prg_name_length + 7;
	  
	  if (*equal == '=')
	    gerd_debug_flags |= g_parse_debug_string (equal + 1, gerd_debug_keys, gerd_n_debug_keys);
	  else if (i + 1 < argc)
	  {
	    gerd_debug_flags |= g_parse_debug_string (argv[i + 1], gerd_debug_keys, gerd_n_debug_keys);
	    argv[i] = NULL;
	    i += 1;
	  }

	  argv[i] = NULL;
	}
    else if (strncmp (argv[i], "--", 2) == 0 &&
	         strncmp (argv[i] + 2, prg_name, prg_name_length) == 0 &&
	         strncmp (argv[i] + 2 + prg_name_length, "-disable", 8) == 0 &&
	         (argv[i][2 + prg_name_length + 8] == 0 || argv[i][2 + prg_name_length + 8] == '='))
    {
	  gchar *equal = argv[i] + 2 + prg_name_length + 8;
	  
	  if (*equal == '=')
	    gerd_debug_flags &= ~g_parse_debug_string (equal + 1, gerd_debug_keys, gerd_n_debug_keys);
	  else if (i + 1 < argc)
	  {
	    gerd_debug_flags &= ~g_parse_debug_string (argv[i + 1], gerd_debug_keys, gerd_n_debug_keys);
	    argv[i] = NULL;
	    i += 1;
	  }

	  argv[i] = NULL;
	}
    else if (strncmp ("--gerd-play", argv[i], 11) == 0)
	{
	  gchar *equal = argv[i] + 11;

	  g_free (gerd_ifile);
	  gerd_ifile = NULL;
	  if (*equal == '=')
	    gerd_ifile = g_strdup (equal + 1);
	  else if (i + 1 < argc)
	  {
	    gerd_ifile = g_strdup (argv[i + 1]);
	    argv[i] = NULL;
	    i += 1;
	  }
        argv[i] = NULL;
	}
    else if (strncmp ("--gerd-record", argv[i], 13) == 0)
	{
	  gchar *equal = argv[i] + 13;

      g_free (gerd_ofile);
	  gerd_ofile = NULL;
	  if (*equal == '=')
	    gerd_ofile = g_strdup (equal + 1);
	  else if (i + 1 < argc)
	  {
	    gerd_ofile = g_strdup (argv[i + 1]);
	    argv[i] = NULL;
	    i += 1;
	  }
	  argv[i] = NULL;
	}
    else if (g_ascii_strcasecmp ("--gerd-help", argv[i]) == 0)
	{
	  print_blurb (stderr, TRUE);
	  argv[i] = NULL;
	  exit (0);
    }
    else if (g_ascii_strcasecmp ("--gerd-version", argv[i]) == 0)
    {
	  print_blurb (stderr, FALSE);
      argv[i] = NULL;
      exit (0);
    }
  }  
  
  e = 0;
  for (i = 1; i < argc; i++)
  {
    if (e)
    {
	  if (argv[i])
	  {
	    argv[e++] = argv[i];
	    argv[i] = NULL;
	   }
	}
    else if (!argv[i])
	  e = i;
  }

  if (e)
    *argc_p = e;
}

static void
print_blurb (FILE    *fout,
	     gboolean print_help)
{
  static const gchar *PRG_NAME = "Gerd";
  static const gchar *PKG_VERSION = "Bloody";
  static const gchar *PKG_VERSION_HINT = "Unstable";
  static const gchar *PKG_NAME = "GERD";
  static const gchar *PKG_HTTP_HOME = "http://people.redhat.com/~timj/gerd";
  
  if (!print_help)
  {
    fprintf (fout, "%s version %s (%s)\n", PRG_NAME, PKG_VERSION, PKG_VERSION_HINT);
    fprintf (fout, "Libraries: ");
    fprintf (fout, "GTK+ %u.%u.%u", gtk_major_version, gtk_minor_version, gtk_micro_version);
    fprintf (fout, ", GLib %u.%u.%u", glib_major_version, glib_minor_version, glib_micro_version);
    fprintf (fout, "\n");
    fprintf (fout, "%s comes with ABSOLUTELY NO WARRANTY.\n", PRG_NAME);
    fprintf (fout, "You may redistribute copies of %s under the terms of\n", PRG_NAME);
    fprintf (fout, "the GNU General Public License which can be found in the\n");
    fprintf (fout, "%s source package. Sources, examples and contact\n", PKG_NAME);
    fprintf (fout, "information are available at %s\n", PKG_HTTP_HOME);
  }
  else
  {
    GSList *slist, *key_list = NULL;
    guint i;

    fprintf (fout, "Usage: gerd-{play|record} <eventfile> <gtkprogram> [gerd-options] [programm-options]\n");
    fprintf (fout, "Gerd Options:\n");
    fprintf (fout, "  --gerd-enable=keys      enable certain features\n");
    fprintf (fout, "  --gerd-disable=keys     disable certain features\n");
    fprintf (fout, "  --gerd-play=<ifile>     playback events from <ifile>\n");
    fprintf (fout, "  --gerd-record=<ofile>   record events to <ofile>\n");
    fprintf (fout, "  --gerd-help             show this help message\n");
    fprintf (fout, "  --gerd-version          print version informations\n");
    fprintf (fout, "Default Features:");

    for (i = 0; i < gerd_n_debug_keys; i++)
	    if (gerd_debug_flags & gerd_debug_keys[i].value)
	      fprintf (fout, "%s %s", i == 0 ? "" : ",", gerd_debug_keys[i].key);
	    else
    	  key_list = g_slist_append (key_list, gerd_debug_keys[i].key);

    fprintf (fout, "\n");
    fprintf (fout, "Other Features:");

    for (slist = key_list; slist; slist = slist->next)
   	  fprintf (fout, "%s %s", slist == key_list ? "" : ",", (gchar*)slist->data);

    g_slist_free (key_list);
    fprintf (fout, "\n");
    fprintf (fout, "Events for <gtkprogram> are either recorded to or played back from\n");
    fprintf (fout, "<eventfile>, depending on whether <ifile> or <ofile> got specified.\n");
    fprintf (fout, "gerd-play and gerd-record are convenience wrapper scripts to start\n");
    fprintf (fout, "<gtkprogram> with the Gerd module.\n");
    fprintf (fout,"[For the 1.2.x versions of Gtk+, they also provide a preloaded helper library.]\n");
  }
}


/* --- Gerd GTK+ Module stuff --- */
G_MODULE_EXPORT const gchar* g_module_check_init (GModule *module);
const gchar*
g_module_check_init (GModule *module)
{
  GModule *main_module;
  gchar *version_check = NULL;

  main_module = g_module_open (NULL, 0);
  if (!main_module)
    return "no main handle";
  if (!g_module_symbol (main_module, "gtk_major_version", (gpointer*) &version_check) && version_check)
    return "no gtk library?";

  version_check = gtk_check_version (GTK_MAJOR_VERSION,
				     GTK_MINOR_VERSION,
				     GTK_MICRO_VERSION - GTK_INTERFACE_AGE);

/*   if (version_check)
    return version_check; 
*/

  g_module = module;

  /* make ourselves resident */
  g_module_open (g_module_name (module), G_MODULE_BIND_LAZY);

  return NULL;
}

G_MODULE_EXPORT void gtk_module_init (gint *argc, gchar ***argv);
void
gtk_module_init (gint    *argc,
		 gchar ***argv)
{
  gerd_init (argc, argv);
}
