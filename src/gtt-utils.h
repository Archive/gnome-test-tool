/* Gnome_test_tool
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * This code has been derived from the GERD project by
 * Tim Janik .
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GERD_UTILS_H__
#define __GERD_UTILS_H__

#include	"config.h"
#include	<gdk/gdkx.h>
#include	<gtk/gtk.h>


#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */


/* compatibility stuff
 */
#define GDK_WINDOW_IS_DESTROYED(w) (((GdkWindowPrivate*) (w))->destroyed)
#define GDK_WINDOW_DISPLAY(w) (((GdkWindowPrivate*) (w))->xdisplay)
#define G_STRINGIFY(macro_or_string)    G_STRINGIFY_ARG (macro_or_string)
#define G_STRINGIFY_ARG(contents)       #contents
/* provide a string identifying the current code position */
#ifdef  __GNUC__
#  define G_STRLOC      __FILE__ ":" G_STRINGIFY (__LINE__) ":" __PRETTY_FUNCTION__ "()"
#else
#  define G_STRLOC      __FILE__ ":" G_STRINGIFY (__LINE__)
#endif
#define DEBUG (gerd_debug_flags & GERD_FEAT_VERBOSE ? g_print : (void (*) (const gchar*, ...)) gtk_true)
static inline gpointer
_gerd_datalist_id_remove_no_notify (GData **datalist,
	 			    GQuark  key_id)
{
  gpointer ret_data = g_datalist_id_get_data (datalist, key_id);
  g_datalist_id_remove_no_notify (datalist, key_id);
  return ret_data;
}
#define g_datalist_id_remove_no_notify _gerd_datalist_id_remove_no_notify
gboolean	gdk_window_translate	(GdkWindow    *src_window,
					 GdkWindow    *dest_window,
					 gint         *x,
					 gint         *y);
void		gdk_root_warp_pointer	(gint	       root_x,
					 gint          root_y);
void		gdk_event_set_state	(GdkEvent     *event,
					 guint         state);
void		gdk_root_add_watch	(guint	       events,
					 GdkFilterFunc function,
					 gpointer      data);


/* --- widget -> window lookups --- */
void		gerd_widget_add_window		(GtkWidget	*widget,
						 GdkWindow	*window);
void		gerd_widget_remove_window	(GtkWidget	*widget,
						 GdkWindow	*window);
GdkWindow*	gerd_widget_get_window		(GtkWidget	*widget,
						 guint		 nth);
gint		gerd_widget_get_window_pos	(GtkWidget	*widget,
						 GdkWindow	*window);


/* --- window paths --- */
guint		gerd_container_get_child_pos	(GtkContainer	*container,
						 GtkWidget	*child);
GtkWidget*	gerd_container_get_child	(GtkContainer	*container,
						 guint		 nth);
gchar*		gerd_toplevel_ensure_unique_id	(GtkWidget	*widget);
GtkWidget*	gerd_toplevel_from_unique_id	(const gchar	*unique_id);
GtkWidget*	gerd_toplevel_from_path		(const gchar	*path);
gchar*		gerd_window_create_path		(GdkWindow	*window);
GdkWindow*	gerd_window_from_path		(const gchar	*unique_path);
#define		GERD_IS_NULL_WINDOW_ID(id)	(id[0] == 'N')





#ifdef __cplusplus
#pragma {
}
#endif /* __cplusplus */

#endif /* __GERD_UTILS_H__ */
