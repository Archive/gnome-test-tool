/* Gnome_test_tool
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GTT_COMMON_H__
#define __GTT_COMMON_H__

#include    <gdk/gdkx.h>
#include    <gtk/gtk.h>

/*----Macros----*/
#define GTT_PIPE_NAME		"gttfifo"
#define MAX_EVENT_SIZE		4096
#define MAX_ENV_LEN			256
#define MAX_PIPE_ATTEMPTS	256
#define MIN_WAIT_LIMIT		2
#define MAX_ACCEPT_COUNT	100     
#define GTT_ERROR			-1
#define GTT_EVENT_PASS		1
#define GTT_EVENT_FAIL		0
#define GTT_TIME_OUT		60*15  /* in minutes */

#define SUCCESS     0
#define FAILURE     1

#if 0
#define DO_DEBUG_RECORD
#define DO_DEBUG_PLAYBACK
#define DO_DEBUG_SOCK
#define DO_DEBUG_SOCK_WIN_ID
#define DO_DEBUG_PIPE
#endif

#ifdef DO_DEBUG_READ_IO
  #define DEBUG_READ_IO g_message 
#else
  #define DEBUG_READ_IO ((void (*) (const gchar*, ...)) gtk_true)
#endif

#ifdef DO_DEBUG_PERFORMANCE
  #define DEBUG_PERFORMANCE g_message 
#else
  #define DEBUG_PERFORMANCE ((void (*) (const gchar*, ...)) gtk_true)
#endif

#ifdef DO_DEBUG_SOCK_WIN_ID
  #define DEBUG_SOCK_WIN_ID g_message 
#else
  #define DEBUG_SOCK_WIN_ID ((void (*) (const gchar*, ...)) gtk_true)
#endif

#ifdef DO_DEBUG_SOCK
  #define DEBUG_SOCK g_message 
#else
  #define DEBUG_SOCK ((void (*) (const gchar*, ...)) gtk_true)
#endif

#ifdef DO_DEBUG_PIPE
  #define DEBUG_PIPE g_message
#else
  #define DEBUG_PIPE ((void (*) (const gchar*, ...)) gtk_true)
#endif

#ifdef DO_DEBUG_RECORD
  #define DEBUG_RECORD g_message
#else
  #define DEBUG_RECORD ((void (*) (const gchar*, ...)) gtk_true)
#endif

#ifdef DO_DEBUG_PLAYBACK
  #define DEBUG_PLAYBACK g_message
#else
  #define DEBUG_PLAYBACK ((void (*) (const gchar*, ...)) gtk_true)
#endif

typedef enum 
{
	GTT_EVENT = 257, 
	GTT_REQUEST_PROCESS_NUM,
	GTT_REQUEST_TESTCASE_NUM,
	GTT_REQUEST_SNAPSHOT_NUM,
	GTT_PROCESS_NUM,
	GTT_TESTCASE_NUM,
	GTT_SNAPSHOT_NUM,
	GTT_MODULE_QUIT
}GttRecordMesgType;

typedef enum 
{       
  GTT_RESPONSE = 1,
	GTT_WIN_ID,
  GTT_SUBTEST,
  GTT_SNAPSHOT_PASS,
  GTT_SNAPSHOT_FAIL,
  GTT_SNAPSHOT_ERROR,
  GTT_RESPONSE_ERROR,
  GTT_VERSION_ERROR,
  GTT_CRASH_ERROR,
  GTT_CROSSING_SUBWINDOW_ERROR,
	GTT_TIME_OUT_ERROR,
	GTT_LAST_EVENT,
  GTT_EXIT,
}GttPlayBackMesgType;

typedef struct
{
    GttRecordMesgType type; 
    gint process_no;
    gchar *event;
}GttRecordMesgFormat;

typedef struct
{
    GttPlayBackMesgType type; 
    gchar *event;
}GttPlayBackMesgFormat;


#endif/* __GTT_COMMON_H__ */
