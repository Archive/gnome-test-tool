/* Gnome_test_tool
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "gtt-common.h"
#include "gtt-playback.h"
#include "gtt-result.h"
#include <sys/types.h>
#include <strings.h>
#include <sys/time.h>
#include <unistd.h>

#include <gdk/gdkx.h>
#include <glib.h>

/* --- prototypes --- */
static gint get_port_number (void);
static gint bind_to_port (gint gttd_sd, gint gttd_port);
static gint wait_for_new_connection (void);
static gint get_sd_for_tag (gint tag, gchar *event);
static gint add_new_sd_to_list (gint new_sd, gint tag, gchar *win_id);
static gint write_event_to_sd (gint sd, gchar *event, gint towrite);
static gint wait_for_rsp (gint sd, gchar **rsp);
static gint check_for_closed_connections (void);
static gint set_all_but_serv_sd (fd_set *read_fd);
static gchar *extract_win_id (gchar *event);
static gchar *read_win_id_from_new_client (gint sd);
static void make_slot_available (gint i);
static gint get_playback_ack (gint tag, gint count);
static GttPlayBackMesgFormat *gtt_frame_playback_message (gchar *resp);

static gint get_next_process_position (gchar **ev, glong *fpos, gint *p_no);
static gboolean wait_for_data_with_timer (gint *sd, gint max_sd, gint secs);
static void gtt_time_out_handler (gpointer data);

/* FIXME: move this to the appropriate header */
#define GTT_PREMATURE_CONNECTION -2

gint gttd_sd = 0;      /* server socket */
gint n_clients = 0;      /* tag - incremented for each connection */
static FILE *result_stream = NULL;
static FILE *fp = NULL;

typedef struct _playback_proc_info
{
  gint sd; 
  gint tag;
  gchar *win_id;  
} pback_proc_info;

pback_proc_info proc_info [MAX_PROCESS];

gint
gttd_comm_init (void)
{
  gint gttd_port;
  gint i;

  gttd_sd = socket (AF_INET, SOCK_STREAM, 0);

  if (gttd_sd < 0)
  {
    g_message ("Value of socket descriptor is less than zero");
    return FAILURE;
  }

  gttd_port = get_port_number ();

  if ((gttd_port = bind_to_port (gttd_sd, gttd_port)) == FAILURE)
  {
    g_message ("Unable to bind the socket ");
    return FAILURE;
  }
  
  DEBUG_PIPE ("Listening in port %d\n", gttd_port);

  listen (gttd_sd, 5);

  for (i = 0; i < MAX_PROCESS; ++i)
  {
    proc_info[i].sd = -1;
    proc_info[i].tag = -1; 
    proc_info[i].win_id = NULL; 
  }
  n_clients = 0;
 
  g_message ((set_sd_to_non_block (gttd_sd) == SUCCESS) ?
    "set_sd_to_non_block success" : "set_sd_to_non_block failure");
 
  return gttd_port;  
}

/* 
 * start with gttd_port, try MAX_BIND_ATTEMPTS times to bind,
 * incrementing gttd_port for each iteration. 
 */
static gint
bind_to_port (gint gttd_sd, gint gttd_port)
{
  struct sockaddr_in serv_addr;
  gint i;
  
  for (i = 0; i < MAX_BIND_ATTEMPTS; ++i, ++gttd_port)
  {
    bzero ((gchar *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl (INADDR_ANY);
    serv_addr.sin_port = htons (gttd_port); 

    if (!bind (gttd_sd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)))
    {
    DEBUG_SOCK ("The bind port no is %d\n", gttd_port);
    return gttd_port;
  }
  }

  return FAILURE;
}

static gint
get_port_number() 
{
  struct servent *servinfo;

  if ((servinfo = getservbyname ("gtt_server", "tcp")))
  {
    return servinfo->s_port;
  }
  perror ("getservbyname");
  DEBUG_SOCK ("Using hardcoded value, therefore: %d \n", SERV_PORT);

  return SERV_PORT;
}


gint 
set_sd_to_non_block (gint sd)
{
  if (fcntl (sd, F_GETFL) > -1)
    if (fcntl (sd, F_SETFL, O_NONBLOCK) > -1)
      return SUCCESS;

  perror ("fcntl");
  return FAILURE;
}
  
static gint 
wait_for_new_connection (void)
{
  struct sockaddr_in cli_addr;
  gint cli_len = sizeof (cli_addr);
  gint new_sd = 0;
  gint i;

  DEBUG_SOCK_WIN_ID ("wait_for_new_connection: %d attempts.. \n", MAX_ACCEPT_COUNT);

  for (i = 0; i < MAX_ACCEPT_COUNT; ++i)
  {
    new_sd = accept (gttd_sd, (struct sockaddr *)&cli_addr, &cli_len);

    if (new_sd > 0)
      break;

    if ((errno == EAGAIN) || (errno == EWOULDBLOCK))
    {
      DEBUG_SOCK ("."); fflush (stdout);
      sleep (1);
    }
    else
    {
      perror ("accept");
      break;
    }
  }
  return new_sd;
}

static gint 
get_sd_for_tag (gint tag, gchar *event)
{
  char *win_id = NULL;
  gint i;
  
  for (i = 0; i < MAX_PROCESS; ++i)
    if (proc_info[i].tag == tag)
      return proc_info[i].sd;

  DEBUG_SOCK ("checking if client was connected by race condition..");

  /* probably the client was around by race condition? */
  win_id = extract_win_id (event);
  for (i = 0; i < MAX_PROCESS; ++i)
  {
    if (proc_info[i].win_id && 
    	!strcmp (win_id, proc_info[i].win_id) && 
		proc_info[i].tag == GTT_PREMATURE_CONNECTION)
    {
      proc_info[i].tag = tag;
      return proc_info[i].sd;
    }
  }
  return -1;
}

static int 
add_new_sd_to_list (gint new_sd, gint tag, gchar *win_id)
{
  gint i;

  DEBUG_SOCK_WIN_ID("add_new_sd_to_list: all_sd[%d] = %d\n", n_clients, new_sd);

  for (i = 0; i < MAX_PROCESS; ++i)
    if (proc_info[i].tag == -1)
      break;

  /* 
   * We don't catch client exit (not always but still..) so there 
   * still might be place in the tbl table for the new connection.
   * If we detect a closed connection, allot it. Otherwise, Chaos!
   */

  if (i == MAX_PROCESS) 
    i = check_for_closed_connections (); 

  if (i == MAX_PROCESS)   /* CHAOS - Ran out of table space !! */
    return FAILURE;

  proc_info[i].tag = tag;
  proc_info[i].sd = new_sd;
  proc_info[i].win_id = win_id;
  return SUCCESS;
}

/*
 * Poll all but server-sd and detect the ones for 0 byte read
 *
 * TODO: Check if there is a possibility of reading from a client
 * and if it's a non-zero read. This can cause CHAOS!!
 */
static gint
check_for_closed_connections ()
{
  gint i;
  gint tot_sd_set;
  gint max_sd;
  fd_set read_fd;
  struct timeval timeout;

  gchar *rsp;
  gint rsp_len;
  gint ret;

  GttPlayBackMesgFormat *playback_mesgformat = NULL;

  max_sd = set_all_but_serv_sd (&read_fd);
  g_assert (max_sd > 0);  /* we should never hit this condition */
    
  /* if within 5 secs we don't have data, all MAX_PROCESS sd-s are open!! */
  timeout.tv_sec = 5;
  timeout.tv_usec = 0;

	for (errno = 0;;errno = 0)
	{
  	tot_sd_set = select (max_sd + 1, &read_fd, 0, 0, &timeout);
		if (errno == EINTR)
			continue;
		else
			break;
	}

  g_assert (tot_sd_set > 0); /* we should never hit this condition */

  for (i = 0; i < MAX_PROCESS; ++i)
  {
    if (FD_ISSET (proc_info[i].sd, &read_fd))
    {
      ret = read (proc_info[i].sd, &rsp_len, sizeof (rsp_len));

      if (ret <= 0)
      {
        make_slot_available (i);
        return i;
      }

      rsp = g_malloc0 (rsp_len);
      ret = read (proc_info[i].sd, rsp, rsp_len);

      if (ret > 0)
      {
        playback_mesgformat = gtt_frame_playback_message (rsp);

        if (playback_mesgformat)
        {
          if (playback_mesgformat->type == GTT_EXIT)
          {
            DEBUG_PLAYBACK ("Received GTT_EXIT");

            make_slot_available (i);

            g_free (playback_mesgformat->event);
            g_free (playback_mesgformat);
            g_free (rsp);

            return i;
          }

          g_free (playback_mesgformat->event);
          g_free (playback_mesgformat);
        }
      }

      g_free (rsp);

      g_message ("FATAL: I read a char from a client when I shouldn't!!\n");
    
      i = MAX_PROCESS; 
      break; 
    }
  }

  g_message ("FATAL: proc_info table exhausted! \n");
  return i;
}

static void
make_slot_available (gint i)
{
	/* close (proc_info[i].sd); */
  proc_info[i].sd = -1;
  proc_info[i].tag = -1; 
  proc_info[i].win_id = NULL; 
	return;
}

static gint
set_all_but_serv_sd (fd_set *read_fd)
{
  gint i;
  gint max_sd;

  FD_ZERO (read_fd);
  for (i = 0, max_sd = -1; i < MAX_PROCESS; ++i)
  {
    if (proc_info[i].sd > 0)
      FD_SET (proc_info[i].sd, read_fd);
    if (proc_info[i].sd > max_sd)
      max_sd = proc_info[i].sd;
  }
  return max_sd;
}

gint 
writen (gint sd, gchar *event, gint towrite)
{
  gint left, written;

  left = towrite;
  while (left)
  {
    written = write (sd, event, left);
    if (written <= 0)
    {
      perror("write");
      return (written);
    }
    left -= written; 
    event += written;
  }
  return (towrite - left);
}

static gint
write_event_to_sd (gint sd, gchar *msg, gint towrite)
{
  DEBUG_SOCK ("socket: writing [%d] to [%d] \n", towrite, sd);

  if (writen (sd, (gchar *)&towrite, sizeof(towrite)))
    if (writen (sd, msg, towrite))
      return SUCCESS;

  perror ("write");
  return FAILURE;
}

gint
readn (gint sd, gchar *ptr, gint bytes_to_read)
{
  gint left, readsofar;

  left = bytes_to_read;
  while (left)
  {
    readsofar = read (sd, ptr, left);
    /*g_message ("Inside readn");*/
    if (readsofar > 0) 
    {
      left -= readsofar; 
      ptr += readsofar;
      continue;
    }

    if (!readsofar)
    {
      g_message ("client closed connection? \n");
      return (readsofar);
    } 

    if ((readsofar < 0) && (errno != EAGAIN) && (errno != EINTR)) 
    {
       /* EAGAIN means no data yet for a non-blocking socket */
      g_message ("client closed connection? \n");
      return (readsofar);
    }

  }
  return (bytes_to_read - left);
}
  
static gboolean
wait_for_data_with_timer (gint *sd, gint max_sd, gint secs)
{
	gint i = 0;
	gint tot_sd_set = -1;
	fd_set read_fd;
  struct timeval timeout;

  timeout.tv_sec = secs;
  timeout.tv_usec = 0;

	FD_ZERO (&read_fd);

	/*for (i = 0; i < max_sd; ++i)*/
		FD_SET (sd[0], &read_fd);

	/*g_message ("Inside wait_for_data_with_timer %d", sd[0]);*/

	for (errno = 0;;errno = 0)
	{
		tot_sd_set = select (max_sd + 1, &read_fd, 0, 0, &timeout);
		if (errno == EINTR)
			continue;
		return ((tot_sd_set > 0) ? TRUE : FALSE);
	}
}

	
static gint 
wait_for_rsp (gint sd, gchar **rsp)
{
  gint rsp_len;
  gint ret;

  if (!wait_for_data_with_timer (&sd, sd, GTT_TIME_OUT))
  {
    gtt_time_out_handler (NULL); 
  }

  ret = readn (sd, (gchar *)&rsp_len, sizeof(rsp_len));
  if (ret <= 0)
  {
    g_message ("Server: readn for first read \n");
    return FAILURE;
  }
  
  *rsp = g_malloc0 (rsp_len);
  
  if(!(*rsp))
  {
    g_message ("Server : Unable to allocate memory");
    return FAILURE;
  }
  
  if (!wait_for_data_with_timer (&sd, sd, GTT_TIME_OUT))
  {
    gtt_time_out_handler (NULL); 
  }

  ret = readn (sd, *rsp, rsp_len);
  if (ret <= 0)
  {
    g_message ("Server: readn for second read \n");
    g_free (*rsp);
    return FAILURE;
  }

  return SUCCESS;
}

/* extract the exact win_id from the event */

static gchar *
extract_win_id (gchar *event)
{
  return event;
}
  
gint
dispatch_event (gchar *event, gint event_len, gint tag, glong fpos)
{
  gchar *win_id = NULL;
  gchar *new_win_id = NULL;
  gint write_sd, new_sd;

  write_sd = get_sd_for_tag (tag, event);

  if (write_sd < 0) /* client not yet connected */
  {
    win_id = extract_win_id (event);

    DEBUG_SOCK_WIN_ID ("%s not in list; %d so wait for connection.. \n", win_id, tag);

    if (!win_id)
    {
      g_message ("Failed to extract the window id");

      return FAILURE;
    }

    /* Loop until you get the right client to avoid the race condition.
       For more info read the README file about race condition */
    for (;;)
    {
      new_sd  = wait_for_new_connection ();

      if (new_sd < 0)
      {
        g_message ("failure: accept new connection \n");

        gtt_playback_stream_printf (result_stream, "Failed to accept new connection\n\n");
        gtt_playback_stream_printf (result_stream, "Application may not be invoked\n\n");
        return FAILURE;
      }

      g_message ((set_sd_to_non_block (new_sd) == SUCCESS) ?
      "set_sd_to_non_block success" : "set_sd_to_non_block failure");

      new_win_id = read_win_id_from_new_client (new_sd);
      if (strcmp (new_win_id, win_id))
      {
        DEBUG_SOCK_WIN_ID ("=====> HIT RACE CONDITION !! ");

        if (add_new_sd_to_list 
            (new_sd, GTT_PREMATURE_CONNECTION, new_win_id) != SUCCESS)
        {
          g_message ("Server: No slot for new client in proc_info table");

          gtt_playback_stream_printf (result_stream, "Exceeded the max number of applications allowed\n\n");
          return FAILURE;
        }
        continue;
      }
      else
      {
        if (add_new_sd_to_list (new_sd, tag, new_win_id) != SUCCESS)
        {
          g_message ("Server: No slot for new client in proc_info table");

          gtt_playback_stream_printf (result_stream, "Exceeded the max number of applications allowed\n\n");

          return FAILURE;
        }
        DEBUG_SOCK_WIN_ID ("Server: %s connection Succeed", win_id);
        break;
      }
    }

    write_sd = new_sd;
  }

  if (write_event_to_sd (write_sd, (gchar *)&fpos, sizeof(fpos)) != SUCCESS)
  {
    g_message ("Server: Failed to write the event");

    gtt_playback_stream_printf (result_stream, "Failed to send the event\n\n");
    gtt_playback_stream_printf (result_stream, "Communication failure\n\n");

    return FAILURE;
  }
  return write_sd;
}

static gchar *
read_win_id_from_new_client (gint sd)
{
  gchar *rsp;
  gchar *win_id = NULL;
  GttPlayBackMesgFormat *mesg_format;

  if (wait_for_rsp (sd, &rsp) != SUCCESS)
    return NULL;

  mesg_format = gtt_frame_playback_message (rsp);

  if (rsp)
  	g_free (rsp);

  if (!mesg_format)
  {
    g_message ("Server : Window id mesg_format is null");
    return NULL;
  }

  if (mesg_format->type != GTT_WIN_ID)
  {
    g_message ("Server : wrong win_id recieved");
    win_id = NULL;
  }
  else
  {
    win_id = g_strdup (mesg_format->event);
  }
  
  g_free (mesg_format->event);
  g_free (mesg_format);
  
  DEBUG_SOCK ("Server : client id : %s",win_id);
  return (win_id);
}

gchar *
get_ack (gint write_sd)
{
  gchar *rsp;
	
  if (wait_for_rsp (write_sd, &rsp) != SUCCESS)
  {
    g_message ("Server: No respone to event dispatch");
    return NULL;
  }
  return rsp;
}

/* frame the response into a GttPlayBackMesgFormat structure. */

static GttPlayBackMesgFormat *
gtt_frame_playback_message (gchar *resp)
{
  gint total_bytes, len = 0;
  GttPlayBackMesgFormat *playback_mesgformat = NULL;

  if (!resp)
    return NULL;

  playback_mesgformat = (GttPlayBackMesgFormat *)
                          g_malloc0 (sizeof (GttPlayBackMesgFormat));

  total_bytes = sizeof (playback_mesgformat->type);
  memcpy ((gchar*) &playback_mesgformat->type, resp, total_bytes);
  resp += total_bytes;

  len = strlen (resp);
  if (len > 0)
  {
    playback_mesgformat->event = (gchar *) g_malloc0 (len + 1);
    strcpy (playback_mesgformat->event, resp);
  }
  else
  {
    playback_mesgformat->event = NULL;
  }

  return playback_mesgformat;
}


/* This function will read the event file and dispatches the events */

void 
gtt_playback_init (gchar *event_file, gchar *result_file)
{

  gint tag = -1; 
  gint ack_sd = -1;	
  gint count = 0;
  gchar *event;
  gint error_flag = FALSE;
  glong fpos = 0;

  /* Open the event file to playback the events */
  fp = fopen (event_file, "r");

  if (!fp)
  {
    g_message ("Error opening event script file %s", event_file);
    return;
  }

    /* Open the result file to store the results of playback */
  result_stream = fopen (result_file, "a");

  if (!result_stream)
  {
    g_message ("Error Opening result file");
    fclose (fp); 
    return;
  }

  memset (event, MAX_EVENT_SIZE, '\0');

  g_message ("Event playback from \"%s\" started", event_file);

  /* Read the events one by one from the event file */
  while (fpos != -1)
  {

    count = get_next_process_position (&event, &fpos, &tag);

    if (fpos == -1)
    {
		  /*error_flag = TRUE;*/
		  break;
	  }
    
    DEBUG_READ_IO ("Process no %d, position %d %s", tag, fpos,event);
    
    if ((ack_sd = dispatch_event (event, strlen (event) + 1, tag, fpos)) == FAILURE) 
    {
      error_flag = TRUE;
      break;
    }
	  g_free (event);
    
    if (get_playback_ack (ack_sd, count) == FALSE)
    {
      error_flag = TRUE;
      break;
    }

  }

  if (error_flag)
  {
    gtt_set_curtest_result (GTT_EVENT_FAIL);
  }
  /* Add the consolidated results in the results file */
  gtt_dump_results (result_stream);

  /* Quit the server after sometime so that the client will quit first */
  sleep (1); 

  fclose (fp); 
  fclose (result_stream);
}

/* This function will receive the acknowlegdement for the dispatched event */

static gint 
get_playback_ack (gint tag, gint count)
{
  gchar *rsp = NULL;

  GttPlayBackMesgFormat *playback_mesgformat = NULL;
  GttPlayBackMesgType msg_type = -1;

  while (msg_type != GTT_LAST_EVENT && msg_type != GTT_EXIT)
  { 
    rsp = get_ack (tag);

    if (!rsp)
    {
      gtt_playback_stream_printf (result_stream, "Null response message received from the application\n\n");

      return FALSE;
    }

    playback_mesgformat = gtt_frame_playback_message (rsp);
    
    g_free (rsp);

    if (!playback_mesgformat || !playback_mesgformat->event) 
    {
      g_message ("Playback Message Format is NULL");

      gtt_playback_stream_printf (result_stream, "Null playback message received from the application\n\n");

      if (playback_mesgformat)
        g_free (playback_mesgformat);
        
      return FALSE; 
    }

    if (gtt_update_results (playback_mesgformat, result_stream) == FAILURE)
    {
      /* Free the allocated memory */
      g_free (playback_mesgformat->event);
      g_free (playback_mesgformat);

      return FALSE; 
    }

    msg_type = playback_mesgformat->type;
	
    /* Free the allocated memory */
    g_free (playback_mesgformat->event);
    g_free (playback_mesgformat);
  }

  DEBUG_READ_IO ("Server : GTT_LAST_EVENT1 received");
  return TRUE;
}

static gint 
get_next_process_position (gchar **ev, glong *fpos, gint *process_no)
{

  gint tag = 0;
  glong prev_fpos = -1;
  gint count=0;
  gchar event[MAX_EVENT_SIZE];

  *fpos = -1;

  prev_fpos = ftell (fp);

  while( (fscanf(fp, "%d %[^\n]", &tag, event)) != EOF)
  {
    count ++;
    if (tag != *process_no)
    {
      *process_no = tag;
      *fpos = prev_fpos;
      *fpos = *fpos == -1 ? 0 : *fpos;
      break;
    }
    prev_fpos = ftell (fp);
  }

  /*g_message ("inside get_next_process_pos %s ", event);*/
  *ev = g_strdup (event);
  return count;
}

/* 
 * FIXME: 
 * Kill the first child
 */
static void
gtt_time_out_handler (gpointer data)
{
  gint i;
  
  gtt_playback_stream_printf (result_stream, "GTT time's out , quiting the application\n\n");

  gtt_set_curtest_result (GTT_EVENT_FAIL);

  gtt_dump_results (result_stream);

  g_message ("Time out occured, so server quitting...");

  for (i = 0; i < MAX_PROCESS; ++i)
    close (proc_info[i].sd);

  fclose (fp);
  fclose (result_stream);
  exit (1);
}


