/* Gerd - GTK+ Event Recorder			-*- C -*-
 * Copyright (C) 2000 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */


/* Gerd Versioning
 */
#define	GERD_VERSION_MAJOR	(1)
#define	GERD_VERSION_REVISION	(1)
#define	GERD_VERSION_PATCHLEVEL	(0)
#define	GERD_VERSION_AGE	(0)
#define	GERD_VERSION		"1.1.0"

