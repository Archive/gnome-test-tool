 /*
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GERD_SNAP_H__
#define __GERD_SNAP_H__

#include        <src/gtt-events.h>


#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */


#define GERD_SNAP_IMAGE_GTT 0
#define GERD_SNAP_IMAGE_PNG 1
#define GERD_SNAP_RESOURCES 2
#define GERD_SNAP_PLAY 0
#define GERD_SNAP_REC 1

/* --- prototypes --- */
void		gerd_snapshot_snp_get	(GdkWindow   *window,
					 gchar       *filename);
void		gerd_snapshot_png_get	(GdkWindow   *window,
					 gchar       *filename);
void		gerd_snapshot_gtt_get	(GdkWindow   *window,
					 gchar       *filename);
void		gerd_snapshot_xwd_get	(GdkWindow   *window,
					 gchar       *filename);
gboolean	gerd_snapshot_xwd_compare (gchar     *basefile,
					   gchar     *newfile);
gboolean	gerd_snapshot_png_compare (gchar     *basefile,
					   gchar     *newfile,
					   GString   **err_str);
gboolean	gerd_snapshot_gtt_compare (gchar     *basefile,
					   gchar     *newfile,
					   GString   **err_str);
gboolean	gerd_snapshot_snp_compare (gchar     *basefile,
					   gchar     *newfile,
					   GString   **err_str);

void		gerd_set_baseline_dir	   (const gchar *dir_name);
void		gerd_set_snapshot_dir	   (const gchar *dir_name);
gchar*		gerd_snapshot_new_filename (gint playrec_type, gint snap_type, gchar * basefile);
void		gerd_snapshot_remove       (gchar *filename);

extern gchar *baseline_dir;
extern gchar *snapshot_dir;

#ifdef __cplusplus
#pragma {
}
#endif /* __cplusplus */

#endif /* __GERD_IO_H__ */

