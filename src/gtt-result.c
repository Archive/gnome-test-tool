/* Gnome_test_tool
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include <stdio.h>
#include <string.h>

#include "gtt-common.h"
#include "gtt-result.h"

/* --- variables --- */
static gchar *current_testname; /* To store the name of the current subtest */
static gint currtest_result; /* To store the status of the current subtest */
static gint subtest_passed, subtest_failed; /* Count of subtest pass/fail */
static gint snapshot_passed, snapshot_failed; /* Count of snapshot pass/fail */

/* --- prototypes --- */
static void gtt_append_subtest_results (gpointer);
static void gtt_append_subtest_details (gpointer, gchar *);
static void gtt_append_snapshot_details (gpointer, gchar *rsp, gint flag);
static void gtt_append_error_details (gpointer, gchar *rsp, gint type);

/* This function will store the results of the subtest in the result file */
static void
gtt_append_subtest_results (gpointer stream)
{
	if (!current_testname)
  {
    current_testname = g_strdup ("test1");
  }	

	if(currtest_result)
	{
		gtt_playback_stream_printf (stream, "SubTest %s\t\t\t: %s\n\n", current_testname, "PASSED");

  	subtest_passed++;
	}
	else
	{
   		gtt_playback_stream_printf (stream, "SubTest %s\t\t\t: %s\n\n", current_testname, "FAILED");

	 	subtest_failed++;
	}

	/* Free the allocated memory */
	if(current_testname)
	{
		g_free(current_testname);
		current_testname = NULL;
	}
}

/* This function will store the subtest details in the result file */

static void 
gtt_append_subtest_details (gpointer stream, gchar *resp)
{
	static gboolean first_time_flag = TRUE;

	if(!first_time_flag)
		gtt_append_subtest_results (stream);

	currtest_result = GTT_EVENT_PASS;
  
	gtt_playback_stream_printf (stream, "Results of Subtest %s\n\n", resp);

	/* Allocate memory to store the current testcase name. Later this will 
	   be used to write the testcase name in the result file */

	current_testname = g_malloc0(strlen(resp));
	g_stpcpy(current_testname, resp);	

	first_time_flag = FALSE;
}

/* This function will store the results of snapshot in the result file */
static void 
gtt_append_snapshot_details (gpointer stream, gchar *rsp, gint flag)
{
	(flag == GTT_SNAPSHOT_PASS) ?  snapshot_passed++ : snapshot_failed++;
	gtt_playback_stream_printf (stream, "%s\n\n", rsp);
}

/* This function will store the error details in the result file */
static void 
gtt_append_error_details (gpointer stream, gchar *rsp, gint type)
{
	if (type != GTT_CROSSING_SUBWINDOW_ERROR)
	  	currtest_result = GTT_EVENT_FAIL;

	if (type == GTT_SNAPSHOT_ERROR)
		snapshot_failed++;

	gtt_playback_stream_printf (stream, "%s\n\n", rsp);
}

/*	This function will invoke corresponding functions depending on the 
	response type to update the results file  */

gint 
gtt_update_results (GttPlayBackMesgFormat *playback_mesg, gpointer stream)
{
	if (!playback_mesg || !playback_mesg->event || !stream)
		return FALSE;

	switch(playback_mesg->type)
	{
		case GTT_SUBTEST:
			gtt_append_subtest_details (stream, playback_mesg->event);
			break;
		case GTT_SNAPSHOT_PASS:
			gtt_append_snapshot_details (stream, playback_mesg->event, GTT_SNAPSHOT_PASS);
			break;
		case GTT_SNAPSHOT_FAIL:
			gtt_append_snapshot_details (stream, playback_mesg->event, GTT_SNAPSHOT_FAIL);
			break;
		case GTT_CROSSING_SUBWINDOW_ERROR:
			break;
		case GTT_SNAPSHOT_ERROR:
			gtt_append_error_details (stream, playback_mesg->event, playback_mesg->type);
			break;
		case GTT_RESPONSE_ERROR:
			gtt_append_error_details (stream, playback_mesg->event, playback_mesg->type);
			g_message ("Received GTT_RESPONSE_ERROR");
			return FAILURE;
		case GTT_CRASH_ERROR:
			gtt_append_error_details (stream, playback_mesg->event, playback_mesg->type);
			g_message ("Received GTT_CRASH_ERROR");
			return FAILURE;
		case GTT_VERSION_ERROR:
			gtt_append_error_details (stream, playback_mesg->event, playback_mesg->type);
			g_message ("Received Version Check Error");
			return FAILURE;
		case GTT_RESPONSE:
			break;
		case GTT_EXIT:
			DEBUG_PLAYBACK ("Received response GTT_EXIT");
			break;
		case GTT_WIN_ID:
			g_message ("Received GTT_WIN_ID");
			break;
		case GTT_LAST_EVENT:
			DEBUG_PLAYBACK ("Received response GTT_LAST_EVENT");
			break;
		default:
			g_message ("Unknown Type\n");
			return FAILURE;
			break;
	}

	return SUCCESS;
}


/* Dump the consolidated results in the result file */

void 
gtt_dump_results (gpointer stream)
{
	/* Update the result for the last test case */
	gtt_append_subtest_results (stream);

	gtt_playback_stream_printf (stream, "Snapshots Passed\t\t: %d\n", snapshot_passed);
	gtt_playback_stream_printf (stream, "Snapshots Failed\t\t: %d\n", snapshot_failed);
	gtt_playback_stream_printf (stream, "Total Snapshots\t\t\t: %d\n\n", (snapshot_failed + snapshot_passed));
	gtt_playback_stream_printf (stream, "Total Subtest Passed\t: %d\n", subtest_passed);
	gtt_playback_stream_printf (stream, "Total Subtest Failed\t: %d\n", subtest_failed);
	gtt_playback_stream_printf (stream, "Total Subtests\t\t\t: %d\n\n", subtest_passed + subtest_failed);

	gtt_playback_stream_printf (stream, "Overall Result\t\t\t: %s\n\n", subtest_failed ? "FAIL" : "PASS");

	g_message ("Overall result:\t%s\n", subtest_failed ? "FAIL" : "PASS");

}

/* Write the playback results in the result file */
void
gtt_playback_stream_printf (gpointer stream, const gchar *format, ...)
{
  FILE *file = stream;
	gchar *string;
	va_list args;

	g_return_if_fail (stream != NULL);
	g_return_if_fail (format != NULL);

	va_start (args, format);
	string = g_strdup_vprintf (format, args);
	va_end (args);

	fputs (string, file);
	fflush (file);
	g_free (string);
}

void 
gtt_set_curtest_result (gint result)
{
 	currtest_result = result;
}
