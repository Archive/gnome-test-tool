/* Gnome_test_tool
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GTT_PLAYBACK_H__
#define __GTT_PLAYBACK_H__

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/time.h>  
#include <netdb.h>

#define SERV_PORT   9499      /* port number */
#define MAX_PROCESS 256

#define MAX_BIND_ATTEMPTS  20    /* for bind */

gint gttd_comm_init (void);
gint set_sd_to_non_block (gint);
gint dispatch_event (gchar *, gint, gint,  glong );
gint readn (gint sd, gchar *ptr, gint bytes_to_read);
gint writen (gint , char *, gint);
char *get_ack (gint);

void gtt_playback_init (gchar *event_file, gchar *result_file);

#endif/* __GTT_PLAYBACK_H__ */
