/* Gnome_test_tool
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * This code has been derived from the GERD project by
 * Tim Janik .
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include	<gtk/gtkinvisible.h>
#include	<string.h>
#include	<stdio.h>
#include	"gtt-utils.h"


/* --- variables --- */
static GQuark quark_window_list = 0;

/* --- functions --- */
gboolean
gdk_window_translate (GdkWindow *src_window,
		      GdkWindow *dest_window,
		      gint      *x,
		      gint      *y)
{
  gint tx = 0, ty = 0;
  gboolean success = FALSE;
  Window child;

  src_window = src_window ? src_window : gdk_get_default_root_window();		
  dest_window = dest_window ? dest_window : gdk_get_default_root_window();	

  if (!GDK_WINDOW_DESTROYED (src_window) && !GDK_WINDOW_DESTROYED (dest_window))
    success = XTranslateCoordinates (GDK_WINDOW_XDISPLAY (src_window),
                GDK_WINDOW_XWINDOW (src_window), 
				GDK_WINDOW_XWINDOW (dest_window), 
				x ? *x : 0, y ? *y : 0, &tx, &ty, &child);

  if (x)
    *x = tx;
  if (y)
    *y = ty;

  return success;
}

void
gdk_root_warp_pointer (gint root_x,
		       gint root_y)
{
  XWarpPointer (GDK_DISPLAY (), None, GDK_ROOT_WINDOW (), 0, 0, 0, 0, root_x, root_y);
}

void
gdk_root_add_watch (guint         events,
		    GdkFilterFunc function,
		    gpointer      data)
{
  GdkWindow *root;

  g_return_if_fail (function != NULL);

  root = gdk_window_lookup (GDK_ROOT_WINDOW ());

  gdk_window_set_events (root, events | gdk_window_get_events (root));
  gdk_window_add_filter (root, function, data);
}

void
gdk_event_set_state (GdkEvent *event,
		     guint     state)
{
  g_return_if_fail (event != NULL);

  switch (event->type)
  {
    case GDK_MOTION_NOTIFY:	event->motion.state = state;	break;
      /* case GDK_SCROLL: */
    case GDK_BUTTON_PRESS:
    case GDK_2BUTTON_PRESS:
    case GDK_3BUTTON_PRESS:
    case GDK_BUTTON_RELEASE:	event->button.state = state;	break;
    case GDK_KEY_PRESS:
    case GDK_KEY_RELEASE:	event->key.state = state;	break;
    case GDK_ENTER_NOTIFY:
    case GDK_LEAVE_NOTIFY:	event->crossing.state = state;	break;
    case GDK_PROPERTY_NOTIFY:	event->property.state = state;	break;
    case GDK_SELECTION_CLEAR:
    case GDK_SELECTION_REQUEST:
    case GDK_SELECTION_NOTIFY:	break;
    case GDK_PROXIMITY_IN:
    case GDK_PROXIMITY_OUT:	break;
    case GDK_DRAG_ENTER:
    case GDK_DRAG_LEAVE:
    case GDK_DRAG_MOTION:
    case GDK_DRAG_STATUS:
    case GDK_DROP_START:
    case GDK_DROP_FINISHED:	break;
    default:			break;
  }
}

static void
widget_unrealize (GtkWidget *widget)
{
   g_object_set_qdata ((GObject*)(GTK_OBJECT (widget)), quark_window_list, NULL);

}

void
gerd_widget_add_window (GtkWidget *widget,
			GdkWindow *window)
{
  GtkWidget *window_widget;

  g_return_if_fail (GTK_IS_WIDGET (widget));
  g_return_if_fail (GTK_WIDGET_REALIZED (widget));
  g_return_if_fail (window != NULL);
  g_return_if_fail (gerd_widget_get_window_pos (widget, window) < 0);	/* paranoid? */

  gdk_window_get_user_data (window, (gpointer) &window_widget);
  if (window_widget != widget)
  {
    g_warning (G_STRLOC ": a widget cannot maintain a foreign window");
    return;
  }

  if (!quark_window_list)
    quark_window_list = g_quark_from_static_string ("GerdWindowList");

  if (!GDK_WINDOW_DESTROYED (window))
  {
    GSList *window_list = g_object_get_qdata ((GObject*) (GTK_OBJECT (widget)),
				       	   quark_window_list); 

    if (window_list)
      window_list = g_slist_append (window_list, window);
    else
    {
      window_list = g_slist_append (window_list, window);

      g_object_set_qdata_full ((GObject*) (GTK_OBJECT (widget)), 
                                quark_window_list, window_list, 
                                  (GDestroyNotify) g_slist_free);
    }

     if( !(g_signal_handler_find((GTK_OBJECT (widget)), 
                                  G_SIGNAL_MATCH_ID | G_SIGNAL_MATCH_FUNC | 
                                  G_SIGNAL_MATCH_DATA , 
                                  g_signal_lookup ("unrealize",GTK_TYPE_WIDGET),
                                  0, 0, widget_unrealize, 
                                  NULL) != 0)  > 0 ) 

	  g_signal_connect (GTK_OBJECT (widget), "unrealize",
	 	                GTK_SIGNAL_FUNC (widget_unrealize), NULL);
    }
}

void
gerd_widget_remove_window (GtkWidget *widget,
			   GdkWindow *window)
{
  GSList *window_list;

  g_return_if_fail (GTK_IS_WIDGET (widget));
  g_return_if_fail (window != NULL);

  /* window_list = g_datalist_id_remove_no_notify (&GTK_OBJECT (widget)->object_data,
						quark_window_list);

  window_list = g_slist_remove (window_list, window);
  g_datalist_id_set_data_full (&GTK_OBJECT (widget)->object_data,
			       quark_window_list,
			       window_list,
			       (GDestroyNotify) g_slist_free); */

  window_list = g_object_get_qdata((GObject*)(GTK_OBJECT (widget)), 
                                    quark_window_list);

  /* gtk_object_remove_data_by_id (GTK_OBJECT (widget), quark_window_list); */

  window_list = g_slist_remove (window_list, window);
  /* gtk_object_set_data_by_id_full (GTK_OBJECT (widget),
			         quark_window_list,
			         window_list,
			         (GDestroyNotify) g_slist_free); */
}

GdkWindow*
gerd_widget_get_window (GtkWidget *widget,
			guint      nth)
{
  GSList *window_list;

  g_return_val_if_fail (GTK_IS_WIDGET (widget), NULL);

  window_list = g_object_get_qdata ((GObject*)(GTK_OBJECT (widget)), 
                                     quark_window_list);

  return g_slist_nth_data (window_list, nth);
}

gint
gerd_widget_get_window_pos (GtkWidget *widget,
			    GdkWindow *window)
{
  GSList *window_list;

  g_return_val_if_fail (GTK_IS_WIDGET (widget), -1);
  g_return_val_if_fail (window != NULL, -1);

  window_list = g_object_get_qdata ((GObject*)(GTK_OBJECT (widget)), 
                                     quark_window_list);

  return g_slist_index (window_list, window);
}

static void
gtk_container_child_position_callback (GtkWidget *widget,
				       gpointer   client_data)
{
  struct {
    GtkWidget *child;
    guint i, index;
  } *data = client_data;

  data->i++;
  if (data->child == widget)
    data->index = data->i;
}

guint
gerd_container_get_child_pos (GtkContainer *container,
			      GtkWidget    *child)
{
  struct {
    GtkWidget *child;
    guint i, index;
  } data;

  g_return_val_if_fail (GTK_IS_CONTAINER (container), ~0);
  g_return_val_if_fail (GTK_IS_WIDGET (child), ~0);
  g_return_val_if_fail (child->parent == GTK_WIDGET (container), ~0);

  data.child = child;
  data.i = 0;
  data.index = ~0;
  gtk_container_forall (container,
			gtk_container_child_position_callback,
			&data);

  return data.index;
}

static void
gtk_container_child_at_position_callback (GtkWidget *widget,
					  gpointer   client_data)
{
  struct {
    GtkWidget *child;
    guint i, index;
  } *data = client_data;

  data->i++;
  if (data->index == data->i)
    data->child = widget;
}

GtkWidget*
gerd_container_get_child (GtkContainer *container,
			  guint         nth)
{
  struct {
    GtkWidget *child;
    guint i, index;
  } data;

  g_return_val_if_fail (GTK_IS_CONTAINER (container), NULL);

  data.child = NULL;
  data.i = 0;
  data.index = nth;
  gtk_container_forall (container,
			gtk_container_child_at_position_callback,
			&data);

  return data.child;
}

static inline guint
normalize_string (gchar *string)
{
  gchar *p;

  for (p = string; *p; p++)
    switch (*p)
    {
      case ' ':		*p = ' ';	break;
      case '\t':	*p = ' ';	break;
      case '|':		*p = '\\';	break;
      case '%':		*p = '#';	break;
    }

  return p - string;
}

static GQuark  quark_toplevel_id = 0;
static GSList *toplevel_list = NULL;

enum {
  TLMATCH_EXACT			= 'E',
  TLMATCH_INVISIBLE		= 'I',
  TLMATCH_NULL			= 'N'
};

GtkWidget*
gerd_toplevel_from_path (const gchar *path)
{
  GtkWidget *toplevel = NULL;
  gchar *toplevel_path, *p;
  gint toplevel_pathlen;

  g_return_val_if_fail (path != NULL, NULL);

  p = strchr (path, '|');

  if (p)
  {
    toplevel_pathlen = p - path;
    toplevel_path = (gchar *) g_malloc ((toplevel_pathlen + 1) * sizeof (gchar));

    strncpy (toplevel_path, path, toplevel_pathlen);
    toplevel_path[toplevel_pathlen] = '\0';

    toplevel = gerd_toplevel_from_unique_id (toplevel_path);

    g_free (toplevel_path);
  }
  return toplevel;
}

GtkWidget*
gerd_toplevel_from_unique_id (const gchar *unique_id)
{
  GtkWidget *toplevel = NULL;
  guint match_type;

  g_return_val_if_fail (unique_id != NULL, NULL);

  match_type = unique_id[0];
  if (match_type && unique_id[1] == ':')
    switch (match_type)
    {
	  GSList *node;
      case TLMATCH_EXACT:
  	    unique_id += 2;

        for (node = toplevel_list; node; node = node->next)
	    {
     	  GtkObject *object = node->data;

	      gchar *id = g_object_get_qdata ((GObject*)object, quark_toplevel_id);

	      if (id && g_ascii_strcasecmp (id + 2, unique_id) == 0)
	      {
		    toplevel = GTK_WIDGET (object);
		    break;
	      }
	    }
	    break;
      case TLMATCH_INVISIBLE:
	    toplevel = gtk_grab_get_current ();
	    if (!GTK_IS_INVISIBLE (toplevel))
	      toplevel = NULL;
	    break;
      case TLMATCH_NULL:
	    toplevel = NULL;
	    break;
    }

  return toplevel;
}

static void
toplevel_unrealize (GtkWidget *widget)
{
  g_object_set_qdata((GObject*) (GTK_OBJECT (widget)), quark_toplevel_id, NULL);

  toplevel_list = g_slist_remove (toplevel_list, widget);
}

gchar*
gerd_toplevel_ensure_unique_id (GtkWidget *widget)
{
  static const gchar *window_types[] = { "Tpl", "Dlg", "Pop", };
  static GData *gerd_name_count = NULL;
  GSList *node, *frags = NULL;
  guint match_type = 0, l = 0;
  gchar *id;

  g_return_val_if_fail (GTK_IS_WIDGET (widget), NULL);
  g_return_val_if_fail (GTK_WIDGET_REALIZED (widget), NULL);

  if (!quark_toplevel_id)
    quark_toplevel_id = g_quark_from_static_string ("GerdToplevelId");

  id = g_object_get_qdata ((GObject*) (GTK_OBJECT (widget)), quark_toplevel_id);

  if (id)
    return id;

  /* figure id parts */
  if (GTK_IS_WINDOW (widget))
  {
    GtkWindow *window = GTK_WINDOW (widget);

    match_type = TLMATCH_EXACT;
    if (GTK_IS_MENU (GTK_BIN (widget)->child))
    {
	  gchar *mtitle = g_object_get_data ((GObject*) (GTK_OBJECT(GTK_BIN (widget)->child)), "gtk-menu-title");

	  frags = g_slist_prepend (frags, g_strdup (mtitle ? mtitle : "NULL-Menu"));
	}
    else
	  frags = g_slist_prepend (frags, 
			                   g_strdup (window->title ? 
									     window->title : "NULL-Title"));

    frags = g_slist_prepend (frags, 
					         g_strdup (window->wmclass_name ? 
									   window->wmclass_name : "NULL-Name"));

    frags = g_slist_prepend (frags, g_strdup (gtk_widget_get_name (GTK_WIDGET (window))));
    frags = g_slist_prepend (frags, g_strdup (window->wmclass_class ? window->wmclass_class : "NULL-Class"));
    frags = g_slist_prepend (frags, g_strdup (window_types[window->type]));
  }
  else if (GTK_IS_INVISIBLE (widget))
  {
    match_type = TLMATCH_INVISIBLE;
    frags = g_slist_prepend (frags, g_strdup (gtk_widget_get_name (widget)));
    frags = g_slist_prepend (frags, g_strdup ("Inv"));
  }
  else
    g_error ("unknown toplevel type");

  /* normalize id parts */
  for (node = frags; node; node = node->next)
  {
    normalize_string (node->data);
    l += strlen (node->data) + 1;
  }

  /* alloc id, preserve space for map count */
  id = g_new (gchar, 2 + 6 + l + 1);

  /* offset by space for map count */
  id += 2 + 6;

  /* add id parts */
  id[0] = 0;
  for (node = frags; node; node = node->next)
  {
    strcat (id, node->data);
    strcat (id, "%");
    g_free (node->data);
  }
  g_slist_free (frags);

  /* figure map count */
  l = GPOINTER_TO_UINT (g_datalist_get_data (&gerd_name_count, id));
  l++;
  g_datalist_set_data (&gerd_name_count, id, GUINT_TO_POINTER (l));

  /* back-offset by space for map count (and match type) */
  id -= 2 + 6;

  /* write out map count and match type and concat with rest */
  g_snprintf (id, 2 + 4 + 1 + 1, "%c:%04x:", match_type, l & 0xffff);
  id[7] = '%';

  g_object_set_qdata_full ((GObject*)(GTK_OBJECT (widget)), quark_toplevel_id, 
                           id, g_free);

  toplevel_list = g_slist_prepend (toplevel_list, widget);


   if( !(g_signal_handler_find((GTK_OBJECT (widget)), 
                                G_SIGNAL_MATCH_ID | G_SIGNAL_MATCH_FUNC | 
                                G_SIGNAL_MATCH_DATA , 
                                g_signal_lookup ("unrealize", GTK_TYPE_WIDGET),
                                0, 0, toplevel_unrealize, 
                                NULL) != 0)  > 0 ) 

    g_signal_connect (GTK_OBJECT (widget), "unrealize",
 		              GTK_SIGNAL_FUNC (toplevel_unrealize), NULL);

  return id;
}

gchar*
gerd_window_create_path (GdkWindow *window)
{
  GSList *node, *frags = NULL;
  GtkWidget *widget;
  GString *gstring;
  gchar *string;
  guint l = 0;

  if (!window)
    return g_strdup_printf ("%c:0000:%%NULL%%", TLMATCH_NULL);

  gdk_window_get_user_data (window, (gpointer) &widget);
 
  if(!GTK_IS_WIDGET (widget) || 
      gerd_widget_get_window_pos (widget, window) == -1) 
  {
    return g_strdup_printf ("%c:0000:%%NULL%%", TLMATCH_NULL);
  }

  string = g_strdup_printf ("|%+d", 
				             gerd_widget_get_window_pos (widget, window));
  l += strlen (string);
  frags = g_slist_prepend (frags, string);
  while (widget->parent)
  {
    guint pos = gerd_container_get_child_pos (GTK_CONTAINER (widget->parent), widget);

    string = g_strdup_printf ("|%s %u", g_type_name (GTK_OBJECT_TYPE (widget)), pos);
    l += strlen (string);
    frags = g_slist_prepend (frags, string);
    widget = widget->parent;
  }

  string = g_strdup (gerd_toplevel_ensure_unique_id (widget));
  l += strlen (string);
  frags = g_slist_prepend (frags, string);

  gstring = g_string_sized_new (l + 1);
  for (node = frags; node; node = node->next)
  {
    string = node->data;
    g_string_append (gstring, string);
    g_free (string);
  }

  g_slist_free (frags);

  string = gstring->str;
  g_string_free (gstring, FALSE);

  return string;
}

GdkWindow*
gerd_window_from_path (const gchar *unique_path)
{
  GdkWindow *window = NULL;
  gchar *free_me, *path, *p;

  g_return_val_if_fail (unique_path != NULL, NULL);

  path = g_strdup (unique_path);
  free_me = path;
  p = strchr (path, '|');
  if (p)
  {
    GtkWidget *widget;

    *p++ = 0;

    widget = gerd_toplevel_from_unique_id (path);

    if (widget)
	{
	  path = p;
	  p = strchr (path, '|');

	  while (widget && p)
	  {
	    gchar tname[128] = { 0, };
	    gint pos = -1;

	    *p++ = 0;
	    sscanf (path, "%127s %d", tname, &pos);
	    widget = GTK_IS_CONTAINER (widget) ? gerd_container_get_child (GTK_CONTAINER (widget), pos) : NULL;

	    if (widget && !g_type_is_a (GTK_OBJECT_TYPE (widget), g_type_from_name (tname)))
	  	  widget = NULL;

	    path = p;
	    p = strchr (path, '|');
	  }

	  if (widget)
	  {
	    gint pos = -1;

	    sscanf (path, "%d", &pos);
	    if (pos >= 0)
	  	  window = gerd_widget_get_window (widget, pos);
	  }
	}
  }

  g_free (free_me);

  return window;
}
