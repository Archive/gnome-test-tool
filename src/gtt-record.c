/* Gnome_test_tool
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include	<stdio.h>  
#include	<unistd.h>
#include	<stdlib.h>
#include	<string.h>
#include	<sys/types.h> 
#include	<sys/stat.h> 
#include	<fcntl.h> 
#include	<signal.h> 
#include	<gdk/gdk.h>

#include	"gtt-common.h"
#include	"gtt-record.h"
#include	"gtt-playback.h"

/* --- prototypes --- */
static	gchar	*gtt_recieve_event (void);
static	GttRecordMesgFormat	*gtt_frame_mesg_format (gchar *mesg);
static	gchar *gtt_create_pipe (void);  

/* --- variables --- */
static FILE *fptr = NULL;
static gchar *event_pipe_name = NULL;
static gchar *seq_pipe_name = NULL;
static gint read_fd, write_fd;

/*	This function will open the event file to record the generated events  
	and also open pipes for communication */

gint
gtt_record_init (gchar *event_file) 
{ 
	if (!event_file)
	{
		g_message ("Server: Can't find event file %s\n", event_file);
		return FAILURE;
	}

	fptr = fopen (event_file, "w"); 
	
	if (!fptr)
	{
		g_message ("Server: Can't open event file %s\n", event_file);
		return FAILURE;
	}

	if ((event_pipe_name = gtt_create_pipe ()) == NULL)
	{
		g_message  ("Server: Unable to create event pipe");
		return FAILURE;
	}
	
	if ((seq_pipe_name = gtt_create_pipe ()) == NULL)
	{
		g_message  ("Server: Unable to create seqeunce pipe");
		return FAILURE;
	}

	read_fd = open (event_pipe_name, O_RDWR); 
	
	if (read_fd < 0)
	{
		g_message  ("Server: Can't open sequence read pipe");
		return FAILURE;
	}

 	write_fd = open (seq_pipe_name, O_RDWR);

	if (write_fd < 0)
	{
		g_message  ("Server: Can't open event write pipe");
		return FAILURE;
	}

	return SUCCESS;
}

void
gtt_record_start (void)
{

	gchar *mesg;
	GttRecordMesgFormat *mesg_format;
	gint next_app_count = 0; /* starts with 0 */
	gint testcase_count = 1; /* starts with 1 */
	gint snapshot_count = 1; /* starts with 1 */
	gint gtt_module_quit_count = 0;

	DEBUG_RECORD  ("GTT Recorder sucessfully started\n");

	do
	{
		mesg = gtt_recieve_event ();

		if (!mesg)
		{
			g_message ("Null message received");
			break;
		}
		
		mesg_format = gtt_frame_mesg_format (mesg);

		g_free (mesg);

		if (!mesg_format)
		{
			g_message (" Null message format received");
			break;
		}
	
		switch (mesg_format->type)
		{
			case GTT_REQUEST_PROCESS_NUM:
				DEBUG_RECORD ("Sending process sequence number");
				mesg_format->type = GTT_PROCESS_NUM;
				writen (write_fd, (gchar *) &mesg_format->type, 
						sizeof (mesg_format->type));
				writen (write_fd, (gchar *) &next_app_count, 
						sizeof (mesg_format->type) );
				next_app_count++;
				break;
			case GTT_REQUEST_TESTCASE_NUM:
				DEBUG_RECORD ("Sending test case number");
				mesg_format->type = GTT_TESTCASE_NUM;
				writen (write_fd, (gchar *) &mesg_format->type, 
						sizeof (mesg_format->type));
				writen (write_fd, (gchar *) &testcase_count, 
						sizeof (testcase_count) );
				testcase_count++;
				break;
			case GTT_REQUEST_SNAPSHOT_NUM:
				DEBUG_RECORD ("Sending snapshot number");
				mesg_format->type = GTT_SNAPSHOT_NUM;
				writen (write_fd, (gchar *) &mesg_format->type, 
						sizeof (mesg_format->type));
				writen (write_fd, (gchar *) &snapshot_count, 
						sizeof (mesg_format->type) );
				snapshot_count++;
				break;
			case GTT_EVENT:
				DEBUG_RECORD ("Framed Event is : %s", mesg_format->event);
				fprintf (fptr,"%d %s", mesg_format->process_no, 
						mesg_format->event);
				break;
			case GTT_MODULE_QUIT:
				DEBUG_RECORD ("Recieved GTT_MODULE_QUIT");
				gtt_module_quit_count ++;
				break;
			default:
				DEBUG_RECORD ("Error: Unknow Msg type");
				break;
		}
		
		if (mesg_format->event)
			g_free (mesg_format->event);

		if (mesg_format)
			g_free (mesg_format);

		mesg_format = NULL;

	} while (gtt_module_quit_count != next_app_count); 

	gtt_close_pipe ();
	g_message ("Event Recorder stopped");
	
}

/*	This function will receive the events generated from the various 
	process via IPC */ 

static gchar* 
gtt_recieve_event (void)
{
	gint event_len = 0, total_bytes = 0; 
	gchar *event = NULL;

	total_bytes = sizeof (event_len);
	if (readn (read_fd, (gchar *) &event_len, total_bytes) != total_bytes)
    {
		g_message ("Server: Unable to receive length of the event");
		return NULL;
	}

	event = (gchar *) g_malloc0 (event_len);
		
	if(!event)
	{
		g_message ("Server: Not enough memory");
		return NULL;
	}	

	if(readn (read_fd, event, event_len) != event_len) 
  {
		g_message ("Server: Error reading the events");
		return NULL;
	}

	return event;
}

/*	This function will frame the the received message into a GttRecordMesgFormat
	structure. This structure will be used for parsing the received message */

static GttRecordMesgFormat * 
gtt_frame_mesg_format (gchar *mesg)
{
	gint total_bytes, len = 0;
	GttRecordMesgFormat * mesg_format;
	
	if (!mesg)
		return NULL;

	mesg_format = (GttRecordMesgFormat *) malloc (sizeof (GttRecordMesgFormat));
	
	total_bytes = sizeof (mesg_format->type);
	memcpy ((gchar *) &mesg_format->type, mesg, total_bytes);
	mesg += total_bytes;
	
	DEBUG_RECORD ("msg type is %d\n",mesg_format->type);
	
	total_bytes = sizeof (mesg_format->process_no);
	memcpy ((gchar *)&mesg_format->process_no, mesg, total_bytes);
	mesg += total_bytes;
	
	DEBUG_RECORD  ("process no is %d\n",mesg_format->process_no);

	len = strlen (mesg); 
	if (len > 0)
	{
		mesg_format->event = (gchar *) g_malloc0 (len + 1); 
		strcpy (mesg_format->event, mesg);
	}
	else
	{
		mesg_format->event = NULL;
	}

	return mesg_format;
}

/*	This function will close all the opened descriptors and also 
	remove the created pipes */

void 
gtt_close_pipe ()
{
	if (read_fd)
		close (read_fd );

	if (write_fd)
	close (write_fd);
	
	if (fptr)
		fclose (fptr);

	if (event_pipe_name)
	{
		unlink (event_pipe_name);
		g_free(event_pipe_name);
	}

	if (seq_pipe_name)
	{
		unlink (seq_pipe_name);
		g_free(seq_pipe_name);
	}
}

static gchar * 
gtt_create_pipe (void) 
{
	gint i;
	gchar pipe_name [256];
	
	for (i = 1;i <= MAX_PIPE_ATTEMPTS; ++i)
	{
		sprintf (pipe_name, "%s/%s%02d", (gchar *) 
					g_getenv ("GERD_WORKING_DIR"), GTT_PIPE_NAME, i);

		if (!mkfifo (pipe_name, 0777))
		{
			DEBUG_RECORD ("Pipe creation passed");
			return g_strdup (pipe_name);
		}
	}
	
	g_message ("Server: Unable to create pipe");
	return NULL;
}

gchar * 
gtt_get_event_pipe_name (void)
{
	return event_pipe_name;
}

gchar * 
gtt_get_seq_pipe_name (void)
{
	return seq_pipe_name;
}
