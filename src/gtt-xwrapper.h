/* Gnome_test_tool
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * This code has been derived from the GERD project by
 * Tim Janik .
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GERD_XWRAPPER_H__
#define __GERD_XWRAPPER_H__

#include        <gdk/gdk.h>


#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */


typedef void (GerdMasqPStateFunc)        (GdkWindow    **window,
					  gint          *win_x,
					  gint          *win_y,
					  gint          *root_x,
					  gint          *root_y,
					  guint         *mask);

#define	GERD_SET_PSTATE_WRAPPER(func)	G_STMT_START { \
  g_dataset_set_data ( (gconstpointer) gdk_event_handler_set, \
                      GERD_KEY_PSTATE_WRAPPER, \
                      (gpointer) (func)); \
} G_STMT_END
  
#define	GERD_KEY_PSTATE_WRAPPER		"GerdPStateWrapper"



#ifdef __cplusplus
#pragma {
}
#endif /* __cplusplus */

#endif /* __GERD_XWRAPPER_H__ */
