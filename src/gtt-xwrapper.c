/* Gnome_test_tool
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * This code has been derived from the GERD project by
 * Tim Janik .
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include	"gtt-xwrapper.h"


#include	<dlfcn.h>
#include	<gdk/gdkx.h>

#ifndef RTLD_NEXT
#  define RTLD_NEXT ((void*) (long) -1)
#endif

#ifndef GDK_WINDOW_XID
#define GDK_WINDOW_XID(w) (((GdkWindowPrivate*) (w))->xwindow)
#endif


/* --- functions --- */
Bool
XQueryPointer (Display      *display,
	       Window        w,
	       Window       *root_return,
	       Window       *child_return,
	       gint          *root_x_return,
	       gint          *root_y_return,
	       gint          *win_x_return,
	       gint          *win_y_return,
	       unsigned int *mask_return)
{
  static Bool (*x_query_pointer) (Display*, Window, Window*, Window*, int*, int*, int*, int*, unsigned int*) = NULL;
  Bool retval;
  
  if (!x_query_pointer)
    {
      x_query_pointer = dlsym (RTLD_NEXT, "XQueryPointer");
      if (!x_query_pointer)
	g_error ("Failed to lookup XQueryPointer()");
    }
  
  retval = x_query_pointer (display, w, root_return, child_return,
			    root_x_return, root_y_return,
			    win_x_return, win_y_return,
			    mask_return);
  
  if (retval)
    {
      static GQuark quark_pstate_wrapper = 0;
      GerdMasqPStateFunc *masq_func;
      
      if (!quark_pstate_wrapper)
	quark_pstate_wrapper = g_quark_from_static_string (GERD_KEY_PSTATE_WRAPPER);
      
      masq_func = g_dataset_id_get_data ((gconstpointer)gdk_event_handler_set, quark_pstate_wrapper);
      if (masq_func)
	{
	  GdkWindow *gwin = gdk_window_lookup (*child_return);
	  
	  masq_func (&gwin, win_x_return, win_y_return, root_x_return, root_y_return, mask_return);
	  
	  if (gwin)
	    *child_return = GDK_WINDOW_XID (gwin);
	  else
	    *child_return = None;
	}
    }
  
  return retval;
}
