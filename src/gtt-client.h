/* Gnome_test_tool
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GTT_CLIENT_H__
#define __GTT_CLIENT_H__

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <netdb.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h> 
#include <signal.h>

#define GTT_IPC_FAILURE	-1 
#define GTT_IPC_SUCCESS	1
#define GTT_IPC_EAGAIN	2

/* --- prototypes --- */
gint gtt_module_record_init (void);
gint gtt_module_record_event (char *event, GttRecordMesgType type);
gint gtt_module_connect_server (void);
gint gtt_module_receive_event (glong *fpos);
gint gtt_module_send_result (GttPlayBackMesgType type, gchar *str);	
gint gtt_module_send_win_id (gchar *str);	
void gtt_module_close_pipe (void);
gint gtt_module_get_process_count (void);
gint  gtt_module_read_info (GttRecordMesgType type);

#endif /* __GTT_CLIENT_H__ */
