/* Gnome_test_tool
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "gtt-common.h"
#include "gtt-playback.h"
#include "gtt-client.h"

/* --- prototypes --- */
static gint gtt_module_get_port_number (void);
static gint gtt_module_send_mesg (GttRecordMesgFormat *mesg_format);
static gint get_serv_ip_addr (char **ip_addr, int *len);
static gchar *gtt_module_get_env_value (gchar *env_var);
static gint readnon_block (gint sd, gchar *ptr, gint bytes_to_read);

/* --- variables --- */
static gchar *event_pipe_name = NULL; /* Name of the pipe for 
										  sending/receiving the events */
static gchar *seq_pipe_name = NULL; /* Name of the pipe to send/receive 
										sequence number (process_count) */
static gint read_fd, write_fd; /* Read, Write file descriptors of pipes */
static gint sd; /* To store the value of socket descriptor */
static gint process_count; /* To store the process count value */

/*	This function is used to get the server port number from the 
	GTT_PORT_NUMBER environmental variable */

static gint 
gtt_module_get_port_number (void)
{
	gchar *str = NULL;
	gint port;

	str = gtt_module_get_env_value ("GTT_PORT_NUMBER");
	port =  str ? atoi (str) : 0;
	g_free (str);
	return port;
}

/*	This function is used for getting the value of the environment variable 
	passed as argument */

static gchar * 
gtt_module_get_env_value (gchar *env_var)
{
	return g_strdup (g_getenv (env_var));
}

/* This function is used for getting the sequence number of the process */

gint
gtt_module_get_process_count (void)
{
	return process_count;
}

/* This function will open the pipes for communication during recording mode */

gint 
gtt_module_record_init (void)
{
	DEBUG_RECORD ("Inside gtt_module_record_init ");

	event_pipe_name = gtt_module_get_env_value ("GTT_EVENT_PIPE"); 
	seq_pipe_name = gtt_module_get_env_value ("GTT_SEQ_PIPE");	
	
	if (!event_pipe_name || !seq_pipe_name)
	{
		g_message ("Pipe name is Null ");
    	return GTT_IPC_FAILURE;
	}

	DEBUG_PIPE ("The event pipe name: %s ", event_pipe_name);
	DEBUG_PIPE ("The sequence pipe name: %s ", seq_pipe_name);

	write_fd = open (event_pipe_name, O_WRONLY);

  	if (write_fd < 0)
  	{
    	g_message ("Client: Unable to open pipe for writing");
    	return GTT_IPC_FAILURE;
	}
	
	read_fd = open (seq_pipe_name, O_RDONLY);	
	
	if (read_fd < 0)
  	{
    	g_message ("Client: Unable to open pipe for reading");
    	return GTT_IPC_FAILURE;
  	}
 	
	/* Get the process sequence number */
	process_count = gtt_module_read_info (GTT_REQUEST_PROCESS_NUM);

	if (process_count == -1)
    {
    	g_message ("Client: Unable to read the process sequence number ");
		return GTT_IPC_FAILURE;
    }
	
	DEBUG_PIPE ("Sequence number sucessfully completed");
	DEBUG_PIPE ("IPC init  completed");

	return GTT_IPC_SUCCESS;
}

/*	This function will create a mesg format for the events generated */

gint
gtt_module_record_event (gchar *event, GttRecordMesgType type)
{
	GttRecordMesgFormat * mesg_format;
	
	mesg_format = (GttRecordMesgFormat *) 
					g_malloc0 (sizeof (GttRecordMesgFormat));	
	mesg_format->type = type;
	mesg_format->process_no = process_count; /* Global variable */
	mesg_format->event = event;

	if (gtt_module_send_mesg (mesg_format) == GTT_IPC_FAILURE)
	{
		g_message ("Client: Unable to send the record message format ");
		g_free (mesg_format);
		return GTT_IPC_FAILURE;
	}
	
	g_free (mesg_format);

	return GTT_IPC_SUCCESS;
}

/* This function is used to send the Mesg format to the server via IPC */

static gint
gtt_module_send_mesg (GttRecordMesgFormat * mesg_format)
{
	gchar *all_str, *start_ptr;
	gint len = 0, total_bytes = 0;
	gint size_type = 0, size_process_no = 0, size_event = 0;  
	
	size_type = sizeof (mesg_format->type);

	DEBUG_RECORD ("mesg type size %d", sizeof (mesg_format->type));
	DEBUG_RECORD ("Process Count number %d", process_count);

	size_process_no = sizeof (mesg_format->process_no);
	
	if (mesg_format->event)
		size_event =  strlen (mesg_format->event) + 1;

	len = size_type + size_process_no + size_event;

	/* type + process_count  + event + null string*/ 

	all_str = g_malloc0 (sizeof (len) + len); 
	start_ptr = all_str;
	
	memcpy (all_str, &len, sizeof (len));
	all_str += sizeof (len); /* move the pointer */ 

	memcpy (all_str, &mesg_format->type, size_type);
	all_str += size_type; /* move the pointer */ 
	
	memcpy (all_str, &process_count, size_process_no);
	all_str += size_process_no; /* move the pointer */ 
	
	if (size_event)	
		g_stpcpy (all_str, mesg_format->event);
	
	DEBUG_RECORD ("Total length written %d",len);

	total_bytes = len + sizeof (len);
	if (writen (write_fd, start_ptr, total_bytes) != total_bytes)
	{
		g_message ("Client: Unable to write the recorded events to the server");
		g_free (start_ptr);
		return GTT_IPC_FAILURE;
	}
	
	g_free (start_ptr);

	return GTT_IPC_SUCCESS;
}

/*	This function requests the server to get either the process count or 
	subtestcase number or snapshot number depending on the given input type */

gint  
gtt_module_read_info (GttRecordMesgType type) 
{
	gint retval;
	gint total_bytes;

	if (type != GTT_REQUEST_PROCESS_NUM && type != GTT_REQUEST_TESTCASE_NUM && 
		type != GTT_REQUEST_SNAPSHOT_NUM )
	{
		g_message ("Client: Invalid type requested ");
    	return -1 ;
	}

	/* Request the server for the snapshot number/process count/subtest number 
	   depending on the type */
	if (gtt_module_record_event (NULL, type) == GTT_IPC_FAILURE)
	{
		g_message ("Client: Error occured in reading");
    	return -1 ;
	}
	
	total_bytes = sizeof (type);
	if ((readn (read_fd, (gchar *) &type, total_bytes) != total_bytes) ||
		(type != GTT_PROCESS_NUM && type != GTT_TESTCASE_NUM  && 
		 type != GTT_SNAPSHOT_NUM))
  	{
    	g_message ("Recieved wrong type");
    	return -1;
	}

	total_bytes = sizeof (retval);
	if (readn (read_fd, (gchar *) &retval, total_bytes) != total_bytes)
  	{
    	g_message ("Unable to get requested number");
    	return -1;
	}

	return retval ;
}

/*	This function will connect the invoked process to the server during 
	playback mode */

gint 
gtt_module_connect_server (void)
{
	gint len;
	gint serv_port = 0, ret_val;
	gint i;
	gchar *serv_ip;
	struct sockaddr_in serv_addr;

	serv_port = gtt_module_get_port_number ();

	if (!serv_port)
	{
		g_message ("Client : Unable to get the server port number");
		return -1;
	}

	DEBUG_PLAYBACK ("The server port number is %d", serv_port);

	bzero ((gchar *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;

	if (get_serv_ip_addr (&serv_ip, &len) == FALSE)
	{
		g_message ("Client: Failed to get the host IpAddress");
		return -1;
	}
	else
	{
		DEBUG_PLAYBACK ("---> SUCCESS with get_ip_addr");

		serv_addr.sin_addr.s_addr = *(long int *)serv_ip;

		/* Free the allocated memory */
		g_free (serv_ip);
	}

	serv_addr.sin_port = htons (serv_port); 

	DEBUG_PIPE ("Client: connecting to server at :%d",serv_port); 

	if ( (sd = socket (AF_INET, SOCK_STREAM, 0)) < 0 )
	{
		perror ("socket");
		return -1;
	}

	if (set_sd_to_non_block (sd) != SUCCESS) 
	{
		g_message ("setting non block mode failed");
		return -1;
	}

	for (i=0; i < MAX_ACCEPT_COUNT; ++i)
	{
		ret_val = connect (sd, (struct sockaddr *) &serv_addr, 
					sizeof (serv_addr));

		if (ret_val != -1) 
			break;

		if ((errno == EAGAIN) || (errno == ETIMEDOUT) || 
			(errno == EINPROGRESS))
    	{
			perror ("connect");
			g_message ("Client : trying for connection");
		    sleep (1);
    	}
		else
		{
			g_message ("Client : connection falied..");
			perror ("connect");
			return -1;
		}

	}

	return sd;
}

/*	This function will receive the events from the server during 
	playback mode */

gint  
gtt_module_receive_event (glong *fpos)
{
	gint len = 0, total_bytes = 0, ret;
	
	total_bytes = sizeof (len);

	ret = readnon_block (sd, (gchar*) &len, total_bytes);

	if (ret == EAGAIN)
	{
		return GTT_IPC_EAGAIN;
	}

	if (ret != total_bytes && len <= 0)
	{
		g_message ("Client: Error recieve event");
		return GTT_IPC_FAILURE;
	}
	
	
	/* Assuming that if received length is 4 bytes then the event will 
	   be ready, so enter in a block mode */

	if (readn (sd, (gchar *)fpos, len) != len)
	{
		g_message (" Client: Error recieve event");
		return GTT_IPC_FAILURE;
	}

	return GTT_IPC_SUCCESS;  
}

/*	This function will send the result of the played event to the server */

gint
gtt_module_send_result (GttPlayBackMesgType rsp_type, gchar *rsp)
{
  gchar *buf = NULL;
  gchar *start_ptr = NULL;
  gint buf_len = 0, total_bytes = 0;

  buf_len = sizeof (rsp_type) + strlen (rsp) + 1;

	/* Allocate memory to store the response and the length of the response */
  buf = g_malloc0 ( sizeof (buf_len) + buf_len);
  start_ptr = buf;

  /* Store the total length of the response */
  memcpy (buf, &buf_len, sizeof (buf_len));
  buf += sizeof (buf_len);

  /* Store the response type */
  memcpy (buf, &rsp_type, sizeof (rsp_type));
  buf += sizeof (rsp_type);

  /* Store the response string */
  g_stpcpy (buf, rsp);

	total_bytes = buf_len + sizeof (buf_len);
  if (writen (sd, start_ptr, total_bytes) != total_bytes)
	{
    	g_free (start_ptr);
		return GTT_IPC_FAILURE;
  }

	/* Free the allocated memory */
  g_free (start_ptr);
  return GTT_IPC_SUCCESS;
}

/* FIXME : optimize this function */

gint
gtt_module_send_win_id (gchar *rsp)
{
  gchar *buf = NULL;
  gchar *start_ptr = NULL;
  gint buf_len = 0, total_bytes = 0;

  buf_len = strlen(rsp) + 1;

  /* Allocate memory to store the response and the length of the response */
  buf = g_malloc0 ( sizeof(buf_len) + buf_len);
  start_ptr = buf;

  /* Store the total length of the response */
  memcpy (buf, &buf_len, sizeof (buf_len));
  buf += sizeof (buf_len);

  /* Store the response string */
  g_stpcpy (buf, rsp);

	total_bytes = buf_len + sizeof (buf_len);
  if (writen (sd, start_ptr, total_bytes) != total_bytes)
	{
    g_free (start_ptr);
		return GTT_IPC_FAILURE;
  }

	/* Free the allocated memory */
  g_free (start_ptr);

  return GTT_IPC_SUCCESS;
}

/*	This function will close the opened file descriptors */

void
gtt_module_close_pipe (void)
{
	if (read_fd)
		close (read_fd );

	if (write_fd)
		close (write_fd);
  
	g_free (event_pipe_name);
	g_free (seq_pipe_name);
}

static gint
get_serv_ip_addr (char **ip_addr, int *len)
{
	char hostname[256];
	struct hostent *hostptr;

	if (!gethostname (hostname, sizeof(hostname)))
	{
		if ((hostptr = gethostbyname (hostname)))
		{
			*len = hostptr->h_length;
			*ip_addr = (char *)malloc (*len);
			memcpy (*ip_addr, *hostptr->h_addr_list, *len);
			return TRUE;
		}
	}
	perror ("get_serv_ip_addr");
	return FALSE;
}

static gint
readnon_block (gint sd, gchar *ptr, gint bytes_to_read)
{
  gint left, readsofar;

  left = bytes_to_read;
  while (left)
  {
    readsofar = read (sd, ptr, left);

    if (readsofar > 0)
    {
      left -= readsofar;
      ptr += readsofar;
      continue;
    }

    if (!readsofar)
    {
      g_message ("Client closed connection? \n");
      return (readsofar);
    }

    if ((readsofar < 0) && (errno != EAGAIN) && (errno != EINTR))
    {
      /* EAGAIN means no data yet for a non-blocking socket */
      g_message ("client closed connection? \n");
      return (readsofar);
    }

    if (errno == EAGAIN || errno == EINTR)
    {
      /*g_message ("Client : readn received EAGAIN mesg");*/
      return EAGAIN;
    }

  }
  return (bytes_to_read - left);
}
