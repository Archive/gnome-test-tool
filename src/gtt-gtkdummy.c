/* Gnome_test_tool
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * This code has been derived from the GERD project by
 * Tim Janik .
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include	<gtk/gtk.h>

/* --- functions --- */
static gboolean
quit_loop (gpointer data)
{
  gtk_main_quit ();

  return FALSE;
}

gint
main (gint    argc,
      gchar **argv)
{
  gtk_init (&argc, &argv);

  g_timeout_add_full (G_PRIORITY_LOW, 0, quit_loop, NULL, NULL);

  gtk_main ();

  return 0;
}
