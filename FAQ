(The FAQ file is getting constantly and rapidly updated so feel free to add)

Frequently Asked Questions about GTT 
====================================

1.	What is a multi-process component and what can GTT do for me?

		Components that invoke (spawn) multiple processes from within 
	        itself. For example, invoking Nautilus (proc - 1) and then
    	        gnome-calculator (proc - 2) from within. So Nautilus is a multi-
		process component. Other examples are gnome-control-center,
		invoking HELP from the applications, etc.
	
		As for what GTT can do for you, see README. In a nut-shell, it
		allows you to automate your GUI testing to a great extent. But
		please see the README to understand even the questions below:)

2.	Can more than one user run gnome-test-tool in parallel ?

		Yes. BUT.. ensure that the options -e, -s and -r specify different
		locations (for storing events, snapshots and result file). Otherwise,
		they will get overwritten! GTT *doesn't* detect two users specifying
		the same file locations!!

3.	Can we specify a different snapshot directory name during playback ?

		Yes. The snapshot directory specified during playing back can be 
		different from the snapshot directory specified during recording. 
		BUT, for comparison of the snapshots, it is neccessary that the 
		snapshot(s) taken during recording should be present in the 
		specified baseline directory. Otherwise the comparison will fail. 
		(baseline directory is the directory where the recorded snapshots 
		are stored)
		
4.	While playing back, what could be the reasons that a snapshot comparison
	might fail ?

		GTT is highly sensitive in this regard, like any automation tool 
		that implements snapshot. Here are some of the scenario that the 
		snapshot comparison is expected to fail: 

		1.	Unable to read the snapshot taken during record mode.

		2.	If the snapshots taken while recording are not present in
			the specified baseline directory.

		3.	If the cursor is flashing while taking snapshots, then the
			snapshots taken during record & playback may differ and in
			that case, the comparison will fail!

		4.	While comparing graphical snaphosts, if the graphical image of 
			the application taken during playback is different from the 
			image taken while recording.

		5.	While comparing object snaphosts, if there are any differences 
			in the snapshot file taken during record & playback.

		6.  Beware: if the snap shot during recording contains dynamic
			data, it's sure to fail during playback! E.g., if the GUI
			contains a timestamp display, it can never be the same for
			two snapshots! Not unless you painfully manipulate the system
			time, which seems to be a theoretical possibility. So always
			expect the snapshot to fail where the GUI has dynamic data!

		7.	The snapshot comparison might fail because of difference in 
			few pixels which is caused by a delay in painting the window. 
			These differences in the pixels are not visible for a user under 
			normal circumstances but the GTT compares each pixel.The best 
			way to ensure maximum success rate of snapshot is by giving 
			sufficient time before taking the snapshot during recording.

5.	Does the GTT stores both the snapshots taken during recording and
	playback?
	
		If the comparison of the snapshots taken during record & playback 
		succeeds then the snapshot taken during playback will be deleted.

		If the comparison of the snapshots taken during record & playback 
		fails then the snapshot taken during playback will not be deleted 
		as it will be useful for manual comparison (debugging).

6.	Where does the snapshots get stored if the snapshot directory is not
	specified while recording/playback ?

		If the snapshot directory is not specified, then the snapshots
		will be taken/stored in the current working directory. The
		rule of thumb is to specify it.

7.	Can the event file be stored in a specific directory ?

		Yes. The event-file can be stored in a specific directory by 
		specifying the directory name along with event-file name. If the
		directory name is not specified, then the event-file will be stored	
		in the current working directory.
	
		gnome-test-tool-record -e /tmp/gcalc001.result gnome-calculator

		The event file will be stored under the /tmp directory.

8.	What if I start GTT without specifying an application to test?

		The GTT will invoke "gnome-terminal" as default application.

9.	Can subtest be added within a given test case ?

	Firstly, what's a subtest? If you got to debug an application by
	secluding some problem, hit F9 key where you think a logical test
	starts and ends. Each F9 is treated as a subtest. When played back,
	the result file will indicate which subtest number failed / passed.
	The number will translate to the n-th F9 key action while recording 
	and help you isolate the problem.

	Yes. We can have many subtests within a given test case. GTT starts 
	recording all events in a file under subtest name "test1" and on 
	using hotkey F9 it starts a new subtest "test2" and starts recording
	the events under this subtest & so on. While playing back, the results
    	of the induvidual subtest will be stored in the results file.

10.	What are the environmental variables that needs to be set before
    starting GTT ?

        The "DISPLAY" environmental variable is to be set before recording
        the events. Some applications behave differently if this DISPLAY
        variable is not set. It is better to set this variable as follows:

        $ export DISPLAY=<ip-addr>:0.0

        If the above one is not working properly, then set the DISPLAY
        as follows:

        $ DISPLAY=localhost:0.0

11. The result file contains some warning messages but the results shows as
    PASSED ?

        For storing the widget information in the file, the GTT will create
        a unique id for every widgets. This unique id creation will be in
        the mapping order. Suppose while recording if the user records the
        events very fastly, while playing back there might be a chance the
        unique id creation getting changed. In this scenario the following
        warning msg might appear in the result file.

        "failed to find window E:0001:....|...|+0".
        ( See TODO file for more info )

        Even though these failure message comes the test case has been 
	treated as PASSED for the following events.

        1. MOTION_NOTIFY
        2. ENTER/LEAVE NOTIFY

        For other events, the test case has been treated as FAILED as it 
        will affect the functionality testing.

        We can avoid this error, by recording the events slowly.

12. While playing back, some errors like "unable to identify crossing
    subwindow..." is appearing in the display.

        This is because while playing back the GTT might not be able to find
        the window in the appications meant for dispatching events.This 
        could be avoided if the events are recorded slowly.

        See FAQ 11.

13. Sometimes GTT doesn't return to the command prompt while recording
    nautilus and needs to quit the GTT using Ctrl-C. Why ?

        The above scenario happens only if the "Use nautilus to draw
        the desktop" option in nautilus is enabled. If the above option
        is enabled then the nautilus process will not be killed completely,
        meaning still some process corresponding to nautilus will be running.
        So the user needs to do Ctrl-C to quit the GTT.

        To avoid this problem, before quitting the nautilus application
        disable the option "Use Nautilus to draw the Desktop".

14. Why key events recorded in Solaris 9 is not working in Solaris 8 ?

		The key event struture has hardware_keycode as a member. While 
		recording GTT stores this hardware_keycode value in the event file. 
		Solaris 8 and 9 generates different hardware_keycode values for 
		the same key event. So key events generated in Solaris 9 will not 
		work in Solaris 8. 

15. How do I report problems ?
	
		As with other components, it is important that you help us reproduce
		the problem. 
	
		If available, preserving the event file and result file would be of 
		help since they specify what went wrong to a decent extent.

16.	How to test Gnome Applets ?
	
		Testing applets is similar to testing applications. But for testing 
		applets, we need to ensure that the environmental variable (GTK_MODULES) 		is inherited by bonobo-activation-server. For this the foll steps needs 
		to be done:

    		1.	From the panel menu, select 
				Applications->Desktop Preferrences->Advanced->Sessions

		    2.	In the Current Session tab, do the following:  

		        Click on gnome-panel.
		        Change the "style" to Normal.
        		Click on Remove  button.
		        Click on Apply button. (Now the panel will be removed)

		    3.	Kill the bonobo-activation-server process. 

        		$ ps -a | grep bonobo-activation-server
		        $ kill -9 (pid)

				Now start recording the events by giving the foll:

			4.	gnome-test-tool-record -e event-file gnome-panel 

    			Panel will start and the applets can be tested. 

		    	Finally quit the panel as in the above steps 1 & 2.

			5.	Now start play back.

		   		gnome-test-tool-play -e event-file gnome-panel

17.	Why playback sometimes fails if the testcase is recorded using 
	the --nowait option ?

	The "nowait" option allows the user to disable the 'real-time'
	record/playback. When this option is used while recording, then the 
	"wait" command will not be added to the event-file. This "wait" is the 
	delay which the user had provided between the events during recording. 

	The "wait" events are not recorded, therefore,  while playing back, the 
	GTT will try to dispatch the events very fast irrespective of whether the 
	widgets get realized or some other tasks(Opening/Saving a file etc) has 
	been fully completed and hence the playback might fail.

18.	Why is a new format called "gtt" introduced and what does it do?
	
	While testing the F5 and F6 features, the snapshots comparisons used 
	to fail quite often thus making the reliability of the tool suspect. 
	After evaluating it is found then taking the pixbuf itself rather than
	converting the snapshots to png format ( where in we can lose some data
	due to compression) provided more reliability to the tool. Hence this
	format is introduced. 

19.	Is there any way where in sacrificing reliability i could still capture
	and save snapshots in png format?
	
	There is a way introduced in the current code through which you can do 
	this however this is not the most efficient way of doing this. If you 
	really want the png formats, please set an environment variable
	GERD_IMAGE_FORMAT to any of these values "png" or "gtt" depending upon
	what you want to choose. This variable needs to be set both during the
	record and the playback mode.
	NOTE : Since the png format is not very reliable, it is better to use i
	the native gtt format for the snapshots capture and saving.

